#!/usr/bin/env python3.5
# -*- coding: UTF-8 -*-

import os, sys
import json

import grpc
from google.protobuf import json_format

sys.path.append(os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))
import maum.brain.han.train.han_train_pb2 as train_pb2
import maum.brain.han.train.han_train_pb2_grpc as train_pb2_grpc
import google.protobuf.empty_pb2 as empty


def main(server, port, type):
    channel = grpc.insecure_channel('{}:{}'.format(server, port))
    client = train_pb2_grpc.HanTrainerStub(channel)

    # create a message from JSON string
    with open('train_client_test.json') as json_data:
        d = json_data.read()
    parsed_pb_model = json_format.Parse(d, train_pb2.HanModel(), ignore_unknown_fields=False)

    d = json.dumps('{"train_id": "7cba43a4-9f4c-40d6-ab46-b8415e2efe0a"}')
    d = json.loads(d)
    parsed_pb_key = json_format.Parse(d, train_pb2.HanTrainKey(), ignore_unknown_fields=False)

    d = json.dumps('{}')
    d = json.loads(d)
    parsed_pb_empty = json_format.Parse(d, empty.Empty(), ignore_unknown_fields=False)

    print("Start {}".format(type))
    if type == "open":  # start train
        return_value = client.Open(parsed_pb_model)
    elif type == "get_progress":   # get train progress
        return_value = client.GetProgress(parsed_pb_key)
    elif type == "get_binary": # todo 기능 확인
        return_value = client.GetBinary(parsed_pb_key)
    elif type == "close":   # delete train id from trainers(list)
        return_value = client.Close(parsed_pb_key)
    elif type == "stop":   # kill train subprocess(cancel)
        return_value = client.Stop(parsed_pb_key)
    elif type == "get_all_progress":   # get all train progress
        return_value = client.GetAllProgress(parsed_pb_empty)
    elif type == "remove_binary":
        return_value = client.RemoveBinary(parsed_pb_key)
    else:
        raise RuntimeError('worng type')
    print(return_value)


if __name__ == '__main__':
    type = sys.argv[1]
    main(server='127.0.0.1', port=45001, type=type)
