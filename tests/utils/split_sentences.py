import argparse
import sys
import os

sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))

from run_client_test import make_nlp_stub, make_in_text


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='split sentences')
    parser.add_argument('-r', '--remote',
                        nargs='?',
                        dest='remote',
                        help='nlp remote',
                        type=str,
                        default='127.0.0.1:9823')
    parser.add_argument('-i', '--input',
                        nargs='?',
                        dest='input',
                        required = True,
                        help='input file',
                        type=str)
    parser.add_argument('-e', '--encoding',
                        nargs='?',
                        dest='encoding',
                        help='input file encoding',
                        type=str,
                        default='utf-8')
    parser.add_argument('--header',
                        nargs='?',
                        dest='header',
                        help='header file',
                        type=str,
                        default='split_header.txt')
    parser.add_argument('-o', '--output',
                        nargs='?',
                        dest='output',
                        help='output file',
                        type=str,
                        default='split_result.txt')

    args = parser.parse_args()

    nlp_client = make_nlp_stub(args.remote)

    nlp_document_list = []
    with open(args.input, 'r', encoding=args.encoding) as rf:
        for text in rf.readlines():
            text = text.replace(u'\xa0', u' ')
            text = text.replace('\n', ' ')
            in_text = make_in_text(text, 1)
            nlp_document = nlp_client.Analyze.future(in_text)
            nlp_document_list.append(nlp_document)

    with open(args.header, 'w', encoding='utf-8') as hf:
        with open(args.output, 'w', encoding='utf-8') as wf:
            for doc_idx, nlp_document in enumerate(nlp_document_list):
                nlp_document = nlp_document.result()
                for sent_idx, sentence in enumerate(nlp_document.sentences):
                    text = sentence.text.strip()
                    hf.write('{}\t{}\n'.format(doc_idx, sent_idx))
                    wf.write('{}\n'.format(text))
