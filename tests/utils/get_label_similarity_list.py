import argparse
import sys
import os

sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))

from run_client_test import ClassifierClient


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='label similarity')
    parser.add_argument('-i', '--ip',
                        nargs='?',
                        dest='ip',
                        help='grpc ip',
                        type=str,
                        default='127.0.0.1')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        help='grpc port',
                        type=int,
                        default=40001)
    parser.add_argument('-o', '--output',
                        nargs='?',
                        dest='output',
                        help='output file',
                        type=str,
                        default='label_similarity.txt')

    args = parser.parse_args()

    print('[connect server]')
    client = ClassifierClient(args.ip, args.port, 1, False)

    print('\n[label_similarity_list]')
    label_similarity_list = client.get_label_similarity_list()
    with open(args.output, 'w', encoding='utf-8') as wf:
        for label_similarity in label_similarity_list.label_similarities:
            wf.write('{}\t{}\t{}\n'.format(
                label_similarity.label1,
                label_similarity.label2,
                label_similarity.similarity))

            print('{}\t{}\t{}'.format(
                label_similarity.label1,
                label_similarity.label2,
                label_similarity.similarity))
