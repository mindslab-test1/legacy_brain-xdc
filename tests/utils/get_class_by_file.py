import argparse
import sys
import os

sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))

from run_client_test import ClassifierClient, make_nlp_stub, make_in_text


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='label similarity')
    parser.add_argument('-r', '--remote',
                        nargs='?',
                        dest='remote',
                        help='nlp remote',
                        type=str,
                        default='127.0.0.1:9823')
    parser.add_argument('-i', '--ip',
                        nargs='?',
                        dest='ip',
                        help='grpc ip',
                        type=str,
                        default='127.0.0.1')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        help='grpc port',
                        type=int,
                        default=40001)
    parser.add_argument('-t', '--top_n',
                        nargs='?',
                        dest='top_n',
                        help='grpc top_n',
                        type=int,
                        default=3)
    parser.add_argument('-f', '--file',
                        nargs='?',
                        dest='input',
                        required = True,
                        help='input file',
                        type=str)
    parser.add_argument('-e', '--encoding',
                        nargs='?',
                        dest='encoding',
                        help='input file encoding',
                        type=str,
                        default='utf-8')
    parser.add_argument('-o', '--output',
                        nargs='?',
                        dest='output',
                        help='output file',
                        type=str,
                        default='result.txt')
    args = parser.parse_args()

    print('[connect server]')
    client = ClassifierClient(args.ip, args.port, args.top_n, False)
    nlp_client = make_nlp_stub(args.remote)
    nlp_document_list = []

    han_result_list = []
    with open(args.input, 'r', encoding=args.encoding) as rf:
        for text in rf.readlines():
            text = text.replace(u'\xa0', u' ')
            text = text.replace('\n', ' ')

            in_text = make_in_text(text, 2)
            nlp_document = nlp_client.Analyze.future(in_text)
            nlp_document_list.append(nlp_document)

    with open(args.output, 'w', encoding='utf-8') as wf:
        for doc_idx, nlp_document in enumerate(nlp_document_list):
            han_result = client.get_class(nlp_document.result())
            wf.write(str(doc_idx))
            for cl in han_result.cls:
                wf.write('\t{}\t{}'.format(cl.label, cl.probability))
            wf.write('\n')
