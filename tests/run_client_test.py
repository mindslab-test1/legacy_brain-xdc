#!/usr/bin/env python3.5
# -*- coding: UTF-8 -*-

import sys
import os
import grpc
import json

from google.protobuf import json_format

sys.path.append(os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))

import maum.brain.han.run.han_run_pb2 as run_pb2
import maum.brain.han.run.han_run_pb2_grpc as run_pb2_grpc
import maum.common.lang_pb2 as lang_pb2
import maum.brain.nlp.nlp_pb2 as brain_nlp_pb2
import maum.brain.nlp.nlp_pb2_grpc as nlp_pb2_grpc
import google.protobuf.empty_pb2 as empty


def make_nlp_stub(remote):
    channel = grpc.insecure_channel(remote)
    return nlp_pb2_grpc.NaturalLanguageProcessingServiceStub(channel)


def make_in_text(text, level):
    in_text = brain_nlp_pb2.InputText()
    in_text.text = text
    in_text.lang = lang_pb2.kor
    in_text.split_sentence = True
    in_text.use_tokenizer = False
    in_text.level = level
    return in_text


class ClassifierClient(object):
    def __init__(self, ip, port, top_n, use_attn_output):
        remote = '{}:{}'.format(ip, port)
        print(remote)
        channel = grpc.insecure_channel(remote)
        self.stub = run_pb2_grpc.HanClassifierStub(channel)

        self.top_n = top_n
        self.use_attn_output = use_attn_output

    def _make_in_document(self, nlp_document):
        in_document = run_pb2.HanInputDocument()

        in_document.document.CopyFrom(nlp_document)

        in_document.common.lang = lang_pb2.kor
        in_document.common.top_n = self.top_n
        in_document.common.use_attn_output = self.use_attn_output

        return in_document

    def get_class_future(self, nlp_document):
        in_document = self._make_in_document(nlp_document)
        return self.stub.GetClass.future(in_document)

    def get_class(self, nlp_document):
        in_document = self._make_in_document(nlp_document)
        return self.stub.GetClass(in_document)

    def _make_in_text(self, text_list):
        in_text = run_pb2.HanInputText()

        in_text.sentences.extend(text_list)

        in_text.common.lang = lang_pb2.kor
        in_text.common.top_n = self.top_n
        in_text.common.use_attn_output = self.use_attn_output

        return in_text

    def get_class_by_text_future(self, text_list):
        in_text = self._make_in_text(text_list)
        return self.stub.GetClassByText.future(in_text)

    def get_class_by_text(self, text_list):
        in_text = self._make_in_text(text_list)
        return self.stub.GetClassByText(in_text)

    def get_label_similarity_list(self):
        return self.stub.GetLabelSimilarityList(empty.Empty())

    def get_structured_feature_list(self):
        return self.stub.GetStructuredFeatureList(empty.Empty())

    def empty_cache(self):
        self.stub.EmptyCache(empty.Empty())


if __name__ == '__main__':
    print('[connect server]')
    client = ClassifierClient('127.0.0.1', 40001, 1, False)

    print('\n[class]')
    nlp_client = make_nlp_stub('127.0.0.1:9823')
    in_text = make_in_text(
        '마이크로소프트와 구글, IT 업계의 거인이 5년에 걸쳐 스마트폰이나 게임기를 둘러싼 수많은 특허 침해 소송 전쟁이 마침내 종결됐다. 양사의 특허 침해 소송은 마이크로소프트가 구글 산하이던 모토롤라모빌리티(Motorola Mobility)의 스마트폰이 자사 보유 특허와 관련한 특정 기능을 사용한다며 기능 사용 정지를 요구하는 금지 소송을 제기하면서 시작됐다. 이에 대해 구글은 마이크로소프트의 게임기 엑스박스에 대해 모토로라가 보유한 특허를 무단 사용했다며 특허 사용료를 요구하며 응전했다. 양사는 미국과 독일 법원에 소송 20건을 제기하는 특허 전쟁 상태에 들어갔다. 하지만 올해 9월 30일 진흙탕 싸움의 양상을 띠던 이 특허 전쟁이 드디어 끝난 것이다. 이에 따라 양사는 특허 침해 소송을 모두 취하했다. 마이크로소프트와 구글이 짧게 낸 공동 성명에는 양사는 특허 분쟁 합의에 도달했다는 점을 기쁘게 생각하며 합의에 따라 양사는 지금까지의 특허 소송을 취하 또는 특정 특허 사항에 대해 서로 협력해 앞으로 사용자 이익에 기여하기로 합의했다고 발표했다. 이번 특허 소송 취하에 대해 양사의 크로스 라이선스 합의가 있었는지 금전적 지급이 있었는지에 대한 정보는 알려지지 않았다. 하지만 마이크로소프트와 구글이 공동 성명을 통해 밝힌 사용자의 이익에 공헌하는 분야는 양사가 아마존, 넷플릭스 등과 결성한 오픈미디어얼라이언스(Alliance for Open Media)의 설립 목적인 차세대 동영상 압축과 전달 기술 개발로 예상해볼 수 있다는 분석이다. 이에 따라 스마트폰이나 PC에서의 스트리밍 콘텐츠를 발전시켜나갈 것으로 기대해볼 수 있다는 것이다. 관련 내용은 이곳에서 확인할 수 있다.',
        2)
    nlp_document = nlp_client.Analyze(in_text)
    label = client.get_class(nlp_document)
    print(json.loads(json_format.MessageToJson(label, True, True)))

    print('\n[class_by_text]')
    label = client.get_class_by_text([
        '마이크로소프트와 구글, IT 업계의 거인이 5년에 걸쳐 스마트폰이나 게임기를 둘러싼 수많은 특허 침해 소송 전쟁이 마침내 종결됐다.',
        '양사의 특허 침해 소송은 마이크로소프트가 구글 산하이던 모토롤라모빌리티(Motorola Mobility)의 스마트폰이 자사 보유 특허와 관련한 특정 기능을 사용한다며 기능 사용 정지를 요구하는 금지 소송을 제기하면서 시작됐다.',
        '이에 대해 구글은 마이크로소프트의 게임기 엑스박스에 대해 모토로라가 보유한 특허를 무단 사용했다며 특허 사용료를 요구하며 응전했다.',
        '양사는 미국과 독일 법원에 소송 20건을 제기하는 특허 전쟁 상태에 들어갔다.',
        '하지만 올해 9월 30일 진흙탕 싸움의 양상을 띠던 이 특허 전쟁이 드디어 끝난 것이다.',
        '이에 따라 양사는 특허 침해 소송을 모두 취하했다.',
        '마이크로소프트와 구글이 짧게 낸 공동 성명에는 양사는 특허 분쟁 합의에 도달했다는 점을 기쁘게 생각하며 합의에 따라 양사는 지금까지의 특허 소송을 취하 또는 특정 특허 사항에 대해 서로 협력해 앞으로 사용자 이익에 기여하기로 합의했다고 발표했다.',
        '이번 특허 소송 취하에 대해 양사의 크로스 라이선스 합의가 있었는지 금전적 지급이 있었는지에 대한 정보는 알려지지 않았다.',
        '하지만 마이크로소프트와 구글이 공동 성명을 통해 밝힌 사용자의 이익에 공헌하는 분야는 양사가 아마존, 넷플릭스 등과 결성한 오픈미디어얼라이언스(Alliance for Open Media)의 설립 목적인 차세대 동영상 압축과 전달 기술 개발로 예상해볼 수 있다는 분석이다.',
        '이에 따라 스마트폰이나 PC에서의 스트리밍 콘텐츠를 발전시켜나갈 것으로 기대해볼 수 있다는 것이다.',
        '관련 내용은 이곳에서 확인할 수 있다.'
    ])
    print(json.loads(json_format.MessageToJson(label, True, True)))

    print('\n[label_similarity_list]')
    label_similarity_list = client.get_label_similarity_list()
    print(json.loads(json_format.MessageToJson(label_similarity_list, True, True)))

    print('\n[structured_feature_list]')
    structured_feature_list = client.get_structured_feature_list()
    print(json.loads(json_format.MessageToJson(structured_feature_list, True, True)))

    print('\n[empty_cache]')
    client.empty_cache()
