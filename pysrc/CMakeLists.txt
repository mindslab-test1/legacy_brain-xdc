cmake_minimum_required(VERSION 2.8.12)

install(DIRECTORY ./maum/brain/han
    DESTINATION lib/python/maum/brain
    FILES_MATCHING PATTERN "*.py")

install(PROGRAMS maum/brain/han/trainerd.py
    DESTINATION bin
    RENAME brain-han-traind
    PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
    )

install(PROGRAMS maum/brain/han/train/han_train_proc.py
    DESTINATION bin
    RENAME brain-han-train-proc
    PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
    )
