#!/usr/bin/env python3.5
# -*- coding: UTF-8 -*-

import torch
import logging
import torch.nn.functional as F

import maum.brain.han.run.han_run_pb2_grpc as run_pb2_grpc
import maum.brain.han.run.han_run_pb2 as run_pb2
import maum.brain.han.train.han_train_pb2 as train_pb2
import maum.brain.han.run.util as util

from maum.brain.han.core.model import ClassifierModel
from maum.brain.han.data.feature import FeatureList
from maum.brain.han.utils.batch import BatchGenerator
from maum.brain.han.utils.error_handler import log_error

logger = logging.getLogger('subproc')


class Runner(run_pb2_grpc.HanClassifierServicer):
    def __init__(self, option: train_pb2.Trainer, state_dict, device):
        self.extra_features = option.raw_data_loader.extra_features

        feature_list = FeatureList('')
        extra_features = util.ExtraFeatureLoader.load(feature_list,
                                                      option.raw_data_loader)
        self.label_list = option.raw_data_loader.labels

        logger.info('[load resources]')
        nlp = util.NlpFactory.make(feature_list, option.nlp)

        logger.info('[load nn model]')
        self.model = ClassifierModel(len(option.raw_data_loader.labels),
                                     feature_list, option.model, state_dict,
                                     device)

        self.batch_generator = BatchGenerator(feature_list)

        self.proto_data_loader = util.ProtoDataLoader(
            feature_list,
            extra_features,
            nlp
        )
        self.device = device

        logger.info('[server has been loaded...]')

    @log_error
    def GetClass(self, in_document, context):
        logger.debug('in_document: %s', in_document)
        han_data = self.proto_data_loader.load_input_document(in_document)

        return self._get_class(han_data,
                               in_document.common.top_n,
                               in_document.common.use_attn_output)

    @log_error
    def GetClassByText(self, in_text, context):
        logger.debug('in_text: %s', in_text)
        han_data = self.proto_data_loader.load_input_text(in_text)

        return self._get_class(han_data,
                               in_text.common.top_n,
                               in_text.common.use_attn_output)

    def _get_class(self, han_data, top_n, use_attn_output):
        torch.cuda.set_device(self.device)
        input_list = self.batch_generator.collate_fn([han_data])

        prediction_tensor, score_tensor, att_result_list = self.model.predict(
            input_list, top_n, use_attn_output)

        ret = run_pb2.HanResult()
        for a_idx in range(top_n):
            label_idx = prediction_tensor[0][a_idx]
            label = self.label_list[label_idx]
            probability = score_tensor[0][a_idx]

            logger.debug('label_idx: %s, label: %s, probability: %s',
                         label_idx, label, probability)
            ret.cls.add(label=label, probability=probability)

        if use_attn_output:
            map_dic = input_list[14][0]
            logger.debug('map_dic: %s', map_dic)
            for s_idx, word_dic in map_dic.items():
                sent_weight = att_result_list[2][0][s_idx]
                sent_attn = ret.sent_attns.add(weight=sent_weight)
                for w_idx, word in word_dic.items():
                    start = word[0]
                    end = word[1]
                    word_weight = att_result_list[1][0][s_idx][w_idx]

                    sent_attn.word_attns.add(weight=word_weight,
                                             start=start,
                                             end=end)

        return ret

    @log_error
    def GetLabelSimilarityList(self, empty, context):
        if self.model.parallel:
            network = self.model.network.module
        else:
            network = self.model.network
        label_weight = network.encoder[-1].module_list[-1].weight.data
        label_weight_1 = label_weight.unsqueeze(2).expand(label_weight.size(0),
                                                          label_weight.size(1),
                                                          label_weight.size(0))
        label_weight_2 = label_weight.transpose(0, 1).unsqueeze(0).expand(label_weight_1.size())

        label_similarities = F.cosine_similarity(label_weight_1, label_weight_2)

        ret = run_pb2.LabelSimilarityList()
        for label_idx1 in range(len(self.label_list)):
            for label_idx2 in range(label_idx1 + 1, len(self.label_list)):
                label1 = self.label_list[label_idx1]
                label2 = self.label_list[label_idx2]
                label_similarity = label_similarities[label_idx1][label_idx2]

                ret.label_similarities.add(label1=label1, label2=label2,
                                           similarity=label_similarity)
        return ret

    @log_error
    def GetStructuredFeatureList(self, empty, context):
        ret = run_pb2.StructuredFeatureList()
        ret.structured_features.extend(self.extra_features)
        return ret

    @log_error
    def EmptyCache(self, empty, context):
        torch.cuda.empty_cache()
        return empty

    @log_error
    def GetLabelList(self, empty, context):
        ret = run_pb2.LabelList()
        self.label_list
        print(self.label_list)
        ret.labels.extend(self.label_list)
        return ret
