import os
import time
import logging
import typing

import maum.brain.han.utils.general as util
import maum.brain.han.data.nlp as nlp
import maum.brain.han.data.feature as feature
import maum.brain.han.data.han_data_pb2 as data_pb2
import maum.brain.han.run.han_run_pb2 as run_pb2

from maum.brain.han.data.han_dataset import HanData


class ProtoDataLoader(object):
    def __init__(self, feature_list: feature.FeatureList,
                 extra_features: typing.Dict[str, feature.Feature],
                 nlp: nlp.NlpBase) -> None:
        self.feature_list = feature_list
        self.extra_features = extra_features
        self.nlp = nlp

    def load_input_text(self, in_text: run_pb2.HanInputText) -> HanData:
        self.nlp.check_support_lang(in_text.common.lang)

        text_list = [sentence for sentence in in_text.sentences]

        feature_data = self.feature_list.make_feature_data_list(0)
        self._load_structured_data(in_text.common.structured_data,
                                   feature_data)

        han_data = HanData(text_list, '', '', feature_data)

        self.nlp.make_feature_one(han_data, in_text.common.use_attn_output, None)
        return han_data

    def load_input_document(self,
                            in_document: run_pb2.HanInputDocument) -> HanData:
        self.nlp.check_support_lang(in_document.common.lang)

        feature_data = self.feature_list.make_feature_data_list(0)
        self._load_structured_data(in_document.common.structured_data,
                                   feature_data)

        han_data = HanData([], '', '', feature_data)

        sentence_list = [[sentence] for sentence in in_document.document.sentences]
        self.nlp.parse_nlp_doc(han_data,
                               sentence_list,
                               in_document.common.use_attn_output)

        return han_data

    def _load_structured_data(self, structured_data: run_pb2.StructuredData,
                              feature_data: feature.FeatureDataList):
        for col_name, extra_feature in self.extra_features.items():
            if type(extra_feature) == feature.ContinuousFeature:
                if extra_feature.level == feature.FeatureLevel.SENTENCE.value:
                    extra_data = structured_data.sentence_data.continuous_datas[col_name].datas
                    extra_data = [[data] for data in extra_data]
                elif extra_feature.level == feature.FeatureLevel.DOC.value:
                    extra_data = structured_data.doc_data.continuous_datas[col_name]
                    extra_data = [extra_data]
                feature_data.input_continuous_data(extra_feature, extra_data)
            elif type(extra_feature) == feature.DiscreteFeature:
                if extra_feature.level == feature.FeatureLevel.SENTENCE.value:
                    extra_data = structured_data.sentence_data.discrete_datas[col_name].datas
                elif extra_feature.level == feature.FeatureLevel.DOC.value:
                    extra_data = structured_data.doc_data.discrete_datas[col_name]
                feature_data.input_discrete_data(extra_feature, extra_data)


class ExtraFeatureLoader(object):
    @staticmethod
    def load(feature_list: feature.FeatureList, option: data_pb2.RawDataLoader):
        extra_feature_list = option.extra_features if option.use_extra_feature else []

        extra_features = {}
        for extra_feature in extra_feature_list:
            name = extra_feature.name
            if extra_feature.level == data_pb2.SENTENCE:
                level = feature.FeatureLevel.SENTENCE
            elif extra_feature.level == data_pb2.DOC:
                level = feature.FeatureLevel.DOC
            else:
                raise RuntimeError('wrong ExtraFeature.level')

            if extra_feature.is_discrete:
                extra_features[name] = feature_list.register_discrete_feature(
                    level, extra_feature.discrete)
            else:
                extra_features[name] = feature_list.register_continuous_feature(
                    level, 1)
        return extra_features


class NlpFactory(object):
    @staticmethod
    def make(feature_list: feature.FeatureList,
             option: data_pb2.Nlp) -> nlp.NlpBase:
        if option.nlp_type == data_pb2.BRAIN_NLP_ENG2:
            return nlp.BrainNlpEng2(feature_list, option)
        elif option.nlp_type == data_pb2.SIMPLE_NLP:
            return nlp.SimpleNlp(feature_list, option)
        elif option.nlp_type == data_pb2.BRAIN_NLP_KOR3:
            return nlp.BrainNlpKor3(feature_list, option)
        else:
            raise RuntimeError('wrong NlpType')


def set_logger(log_dir, level):
    os.environ['TZ'] = 'Asia/Seoul'
    time.tzset()
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    logfile_name = os.path.join(log_dir, '{}_server.log'.format(time.strftime("%Y%m%d_%H%M%S")))
    util.custom_logger(name='subproc', type='file', fn=logfile_name)
    logger = logging.getLogger('subproc')
    if level == 'DEBUG':
        logger.setLevel(logging.DEBUG)
    elif level == 'INFO':
        logger.setLevel(logging.INFO)
    elif level == 'WARNING':
        logger.setLevel(logging.WARNING)
    elif level == 'ERROR':
        logger.setLevel(logging.ERROR)
    elif level == 'CRITICAL':
        logger.setLevel(logging.CRITICAL)
    return logger
