#!/usr/bin/env python3.5
# -*- coding: UTF-8 -*-

import os
import torch
import argparse
import time
import grpc
import sys

from concurrent import futures

sys.path.append(os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))

import maum.brain.han.run.han_run_pb2_grpc as run_pb2_grpc
import maum.brain.han.run.util as util

from maum.brain.han.run.runner import Runner
from common.config import Config

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


if __name__ == '__main__':
    conf = Config()
    conf.init('brain-han-train.conf')

    parser = argparse.ArgumentParser(
        description='classifier runner executor')
    parser.add_argument('-m', '--model',
                        nargs='?',
                        dest='model',
                        required=True,
                        help='Model Path. (prefix: $MAUM_ROOT/trained/han)',
                        type=str)
    parser.add_argument('-l', '--log_level',
                        nargs='?',
                        dest='log_level',
                        help='logger level',
                        type=str,
                        default='INFO')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        help='grpc port',
                        type=int,
                        default=40001)
    parser.add_argument('-d', '--device',
                        nargs='?',
                        dest='device',
                        help='gpu device',
                        type=int,
                        default=0)

    args = parser.parse_args()

    log_dir = os.path.join(conf.get('logs.dir'), 'han', 'server')
    logger = util.set_logger(log_dir, args.log_level)
    logger.info("starting server...")

    pt_path = os.path.join(conf.get('brain-han.trained.root'),
                           args.model)
    checkpoint = torch.load(pt_path, map_location=lambda storage, loc: storage)
    runner = Runner(checkpoint['option'], checkpoint['state_dict'],
                    args.device)

    time_out = int(conf.get("brain-han.runner.front.timeout"))
    data = [
        ('grpc.max_connection_idle_ms', time_out),
        ('grpc.max_connection_age_ms', time_out)
    ]
    max_workers = int(conf.get('brain-han.runner.max.worker'))
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=max_workers),
                         options=data)
    run_pb2_grpc.add_HanClassifierServicer_to_server(runner, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
