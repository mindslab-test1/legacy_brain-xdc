import typing
import logging
import numpy as np

from maum.brain.han.train.util import HanDataLoader

logger = logging.getLogger('subproc')


class CudaErrorHandler(object):
    @staticmethod
    def handle_error(error: RuntimeError, size: typing.List[int],
                    data_loader: HanDataLoader):
        is_out_of_memory = error.args[0].startswith(
            'cuda runtime error (2)')
        is_cudnn_error = error.args[0].startswith(
            'CUDNN_STATUS_NOT_SUPPORTED')
        is_cupy_error = error.args[0].startswith(
            'CUDA_ERROR_INVALID_VALUE')
        if is_out_of_memory or is_cudnn_error:
            max_size_prod = int(np.prod(size) - 1)
            logger.warning('{} is too big size.'.format(size))
            logger.warning('max_size_prod {} -> {}'.format(
                data_loader.max_size_prod,
                max_size_prod))
            data_loader.max_size_prod = max_size_prod
        elif is_cupy_error:
            max_batch_size = size[0] // 2
            logger.warning(
                '{} is too big size. max_batch_size {} -> {}'.format(
                    size,
                    data_loader.max_batch_size,
                    max_batch_size
                ))
            data_loader.max_batch_size = max_batch_size
        else:
            raise error
        data_loader.make()


def log_error(func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            logger.exception(e)
            raise e
    return wrapper
