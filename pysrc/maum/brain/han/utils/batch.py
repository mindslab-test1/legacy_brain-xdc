import itertools
import torch
import typing
import logging
import numpy as np

import maum.brain.han.data.han_dataset as han_dataset
import maum.brain.han.data.feature as feature

logger = logging.getLogger('subproc')


class BatchUtil(object):
    @staticmethod
    def fill_data(tensor, data_list, level: int):
        concat_dim = feature.FeatureLevel.__len__() - 1 - level
        for b_idx, batch in enumerate(data_list):
            feature_data_list = []
            for feature_data in batch:
                feature_data_list.append(feature_data)
            feature_data_tensor = torch.cat(feature_data_list, concat_dim)
            slice_list = [
                slice(0, size, 1)
                for size in feature_data_tensor.size()
            ]
            tensor[b_idx][tuple(slice_list)].copy_(feature_data_tensor)

    @staticmethod
    def make_size(
            batch: typing.List[feature.FeatureDataList]
    ) -> typing.List[int]:
        size = [
            max(feature_data.max_size[level] for feature_data in batch)
            for level in feature.FeatureLevel.get_list_asc()
        ]
        size[0] = len(batch)
        return size


class BatchGenerator(object):
    def __init__(self, feature_list: feature.FeatureList) -> None:
        self.discrete_feature_length = feature_list.make_feature_length(
            feature.FeatureType.DISCRETE)
        self.continuous_feature_length = feature_list.make_feature_length(
            feature.FeatureType.CONTINUOUS)

    def collate_fn(self, batch: typing.List[han_dataset.HanData]):
        batch_data_list = [data.feature_data for data in batch]
        size = BatchUtil.make_size(batch_data_list)

        mask = [
            torch.ByteTensor(
                *size[:feature.FeatureLevel.__len__() - level]
            ).fill_(1)
            for level in feature.FeatureLevel.get_list_asc()
        ]

        doc_level = feature.FeatureLevel.DOC.value
        mask[doc_level].fill_(0)
        for b_idx, feature_data in enumerate(batch_data_list):
            sentence_list = feature_data.size
            sentence_level = feature.FeatureLevel.SENTENCE.value
            mask[sentence_level][b_idx, :len(sentence_list)].fill_(0)
            for s_idx, word_list in enumerate(sentence_list):
                word_level = feature.FeatureLevel.WORD.value
                mask[word_level][b_idx, s_idx, :len(word_list)].fill_(0)
                for w_idx, len_char in enumerate(word_list):
                    char_level = feature.FeatureLevel.CHAR.value
                    mask[char_level][b_idx, s_idx, w_idx, :len_char].fill_(0)

        discrete_data_list = [
            torch.LongTensor(
                *mask[level].size(), self.discrete_feature_length[level]
            ).zero_()
            if self.discrete_feature_length[level] > 0 else None
            for level in feature.FeatureLevel.get_list_asc()
        ]

        for level, discrete_data in enumerate(discrete_data_list):
            if discrete_data is not None:
                data_list = [
                    feature_data.discrete_data_list[level]
                    for feature_data in batch_data_list
                ]
                BatchUtil.fill_data(discrete_data, data_list, level)

        continuous_data_list = [
            torch.FloatTensor(
                *mask[level].size(), self.continuous_feature_length[level]
            ).zero_()
            if self.continuous_feature_length[level] > 0 else None
            for level in feature.FeatureLevel.get_list_asc()
        ]

        for level, continuous_data in enumerate(continuous_data_list):
            if continuous_data is not None:
                data_list = [
                    feature_data.continuous_data_list[level]
                    for feature_data in batch_data_list
                ]
                BatchUtil.fill_data(continuous_data, data_list, level)

        target = torch.LongTensor(
            [feature_data.label for feature_data in batch_data_list]
        )

        uid = [data.uid for data in batch]
        text_list = [data.text_list for data in batch]
        map_dic = [data.map_dic for data in batch]

        return list(
            itertools.chain(discrete_data_list, continuous_data_list, mask,
                            [uid, text_list, map_dic, target, size])
        )


class SortedBatchSampler(object):
    def __init__(self, data_list: typing.List[han_dataset.HanData],
                 max_size_prod: int, max_batch_size: int, shuffle=True) -> None:
        self.shuffle = shuffle

        size_list = np.array([
            [size for size in data.feature_data.max_size] for data in data_list
        ])

        size_np = np.array(
            [(*size, np.random.random()) for size in size_list],
            dtype=[
                ('l1', np.int_), ('l2', np.int_), ('l3', np.int_),
                ('l4', np.int_), ('rand', np.float_)
            ]
        )

        size_index_list = np.argsort(size_np,
                                     order=('l1', 'l2', 'l3', 'l4', 'rand'))

        self.batches = []
        max_size = np.array([0, 0, 0, 0])
        idx_list = []
        for size_index in size_index_list.__iter__():
            size = size_list[size_index, :]
            if max_size_prod < np.prod(size):
                logger.warning('This text is too long to use. (uid: {}, size: {})'
                      .format(data_list[size_index].uid, size))
                continue

            max_size[0] += 1
            max_size = np.maximum(size, max_size)

            if max_size_prod < np.prod(max_size) or max_batch_size <= len(idx_list):
                self.batches.append(idx_list)
                max_size = size
                idx_list = []

            idx_list.append(size_index)

        if len(idx_list) > 0:
            self.batches.append(idx_list)

    def __iter__(self):
        if self.shuffle:
            np.random.shuffle(self.batches)
        for batch in self.batches:
            yield batch

    def __len__(self):
        return len(self.batches)
