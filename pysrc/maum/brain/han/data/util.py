import torch
import collections
import numpy
import typing
import logging

logger = logging.getLogger('subproc')


class FileLoadUtil(object):
    @staticmethod
    def load_voca(dict_file: str) -> typing.Dict[str, int]:
        if dict_file:
            dictionary = collections.defaultdict(lambda: 1)
            dictionary['<pad>'] = 0
            dictionary['<unk>'] = 1

            i = 2
            with open(dict_file, 'r', encoding='utf=8') as f:
                logger.info('loading {}'.format(dict_file))
                for line in f.readlines():
                    token = line.split("\n")[0].lower()
                    if token not in dictionary:
                        dictionary[token] = i
                        i += 1
            return dictionary
        else:
            raise RuntimeError('dictionary path is null')

    @staticmethod
    def load_emb(emb_file: str, emb_dim: int,
                 dictionary: typing.Dict[str, int]) -> torch.FloatTensor:
        voca_dim = len(dictionary)
        embedding = 0.01 * numpy.random.randn(voca_dim, emb_dim)

        with open(emb_file, 'r', encoding='utf=8') as f:
            logger.info('loading {}'.format(emb_file))
            emb_count = 0
            for line in f.readlines():
                elems = line.split()
                token = elems[0].lower()
                if len(elems) == emb_dim + 1 and token in dictionary:
                    emb_idx = dictionary[token]
                    embedding[emb_idx] = [float(v) for v in elems[1:]]
                    emb_count += 1
            if voca_dim != emb_count + 2:
                miss_match = voca_dim - emb_count - 2
                logger.info('voca count {} emb count {} miss match {}'
                      .format(voca_dim, emb_count, miss_match))

        return torch.FloatTensor(embedding)


class NlpUtil(object):
    @staticmethod
    def make_tf_data(word_matrix):
        word_counter = collections.Counter()
        for word_list in word_matrix:
            word_counter.update(word_list)
        total = sum(word_counter.values()) + 1e-5

        return [
           [
               [word_counter[word] / total] for word in word_list
           ] for word_list in word_matrix
        ]
