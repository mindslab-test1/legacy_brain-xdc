import typing
import torch
import os
import collections

from enum import Enum

import maum.brain.han.data.han_data_pb2 as data_pb2

from maum.brain.han.data.util import FileLoadUtil


class FeatureType(Enum):
    DISCRETE = 1
    CONTINUOUS = 2


class FeatureLevel(Enum):
    CHAR = 0
    WORD = 1
    SENTENCE = 2
    DOC = 3

    @classmethod
    def get_list_asc(cls):
        return range(cls.__len__())


class Feature(object):
    def __init__(self, level: FeatureLevel, index: int) -> None:
        self.level = level.value
        self.index = index


class DiscreteFeature(Feature):
    def __init__(self, resource_home: str, level: FeatureLevel, index: int,
                 option: data_pb2.DiscreteFeature) -> None:
        super(DiscreteFeature, self).__init__(level, index)
        if option.dictionary:
            self.dictionary = collections.defaultdict(lambda: 1)
            self.dictionary.update(option.dictionary)
            is_first_train = False
        else:
            dict_file = os.path.join(resource_home, option.dict_file)
            self.dictionary = FileLoadUtil.load_voca(dict_file)
            option.dictionary.update(self.dictionary)
            is_first_train = True

        self.use_emb = option.use_emb
        if self.use_emb:
            pre_train_file = os.path.join(resource_home,
                                          option.emb.pre_train_file)
            self.emb_value = FileLoadUtil.load_emb(
                pre_train_file,
                option.emb.dim,
                self.dictionary
            ) if option.emb.pre_train_file and is_first_train else None
            self.emb_opt = option.emb

    def make_tensor(self, data, max_size: typing.List[int]) -> torch.LongTensor:
        if self.level == FeatureLevel.CHAR.value:
            index_data = [
                [
                    [self.dictionary[token.lower()] for token in token_list]
                    + [0] * (max_size[3] - len(token_list))
                    for token_list in token_matrix
                ] + [[0] * max_size[3]] * (max_size[2] - len(token_matrix))
                for token_matrix in data
            ]
            return torch.LongTensor(index_data).unsqueeze(3)
        elif self.level == FeatureLevel.WORD.value:
            index_data = [
                [self.dictionary[token.lower()] for token in token_list]
                + [0] * (max_size[2] - len(token_list))
                for token_list in data
            ]
            return torch.LongTensor(index_data).unsqueeze(2)
        elif self.level == FeatureLevel.SENTENCE.value:
            return torch.LongTensor(
                [self.dictionary[token.lower()] for token in data]
            ).unsqueeze(1)
        elif self.level == FeatureLevel.DOC.value:
            return torch.LongTensor([self.dictionary[data.lower()]])
        else:
            raise RuntimeError('wrong FeatureLevel')


class ContinuousFeature(Feature):
    def __init__(self, level: FeatureLevel, index: int, dim: int) -> None:
        super(ContinuousFeature, self).__init__(level, index)
        self.dim = dim

    def make_tensor(self, data, max_size: typing.List[int]) -> torch.FloatTensor:
        if self.level == FeatureLevel.CHAR.value:
            index_data = [
                [
                    [token for token in token_list]
                    + [[0] * self.dim] * (max_size[3] - len(token_list))
                    for token_list in token_matrix
                ] + [[[0] * self.dim] * max_size[3]]
                * (max_size[2] - len(token_matrix))
                for token_matrix in data
            ]
            return torch.FloatTensor(index_data)
        elif self.level == FeatureLevel.WORD.value:
            index_data = [
                [token for token in token_list]
                + [[0] * self.dim] * (max_size[2] - len(token_list))
                for token_list in data
            ]
            return torch.FloatTensor(index_data)
        elif self.level in (FeatureLevel.SENTENCE.value, FeatureLevel.DOC.value):
            return torch.FloatTensor(data)
        else:
            raise RuntimeError('wrong FeatureLevel')


class FeatureDataList(object):
    def __init__(self, discrete_data_list, continuous_data_list,
                 label: int, init_max_size: typing.List[int]) -> None:
        self.discrete_data_list = discrete_data_list
        self.continuous_data_list = continuous_data_list

        self.label = label

        self.max_size = init_max_size
        self.size = None

    def input_discrete_data(self, feature: DiscreteFeature, data) -> None:
        index_tensor = feature.make_tensor(data, self.max_size)
        self.discrete_data_list[feature.level][feature.index] = index_tensor

    def input_continuous_data(self, feature: ContinuousFeature, data) -> None:
        tensor = feature.make_tensor(data, self.max_size)
        self.continuous_data_list[feature.level][feature.index] = tensor

    def input_size(self, data) -> None:
        self.size = [
            [len(token) for token in token_list] for token_list in data
        ]

        max_size = [1, len(data), max(len(sentence) for sentence in data)]

        max_word_len_list = []
        for sentence in data:
            max_word_len = max(len(word) for word in sentence)
            max_word_len_list.append(max_word_len)
        max_size.append(max(max_word_len_list))

        self.max_size = [max(min_value, value)
                         for min_value, value in zip(self.max_size, max_size)]


class FeatureList(object):
    def __init__(self, resource_home: str) -> None:
        self.resource_home = resource_home
        self.discrete_feature_list = [
            [] for _ in FeatureLevel
        ]
        self.continuous_feature_list = [
            [] for _ in FeatureLevel
        ]
        self.init_max_size = [0 for _ in FeatureLevel]

    def register_discrete_feature(self, level: FeatureLevel,
                                  option: data_pb2.DiscreteFeature) -> DiscreteFeature:
        index = len(self.discrete_feature_list[level.value])
        feature = DiscreteFeature(self.resource_home, level, index, option)
        self.discrete_feature_list[level.value].append(feature)
        return feature

    def register_continuous_feature(self, level: FeatureLevel,
                                    dim: int) -> ContinuousFeature:
        index = len(self.continuous_feature_list[level.value])
        feature = ContinuousFeature(level, index, dim)
        self.continuous_feature_list[level.value].append(feature)
        return feature

    def make_feature_data_list(self, label: int) -> FeatureDataList:
        discrete_data_list = [
            [''] * len(discrete_feature_list)
            for discrete_feature_list in self.discrete_feature_list
        ]
        continuous_data_list = [
            [''] * len(continuous_feature_list)
            for continuous_feature_list in self.continuous_feature_list
        ]
        return FeatureDataList(discrete_data_list, continuous_data_list, label,
                               self.init_max_size)

    def make_feature_length(self, feature_type: FeatureType) -> typing.List[int]:
        if feature_type == FeatureType.DISCRETE:
            discrete_feature_length = [
                len(self.discrete_feature_list[level])
                for level in FeatureLevel.get_list_asc()
            ]
            return discrete_feature_length
        elif feature_type == FeatureType.CONTINUOUS:
            continuous_feature_length = [
                sum([
                    continuous_feature.dim
                    for continuous_feature
                    in self.continuous_feature_list[level]
                ]) for level in FeatureLevel.get_list_asc()
            ]
            return continuous_feature_length
        else:
            RuntimeError('wrong FeatureType')
