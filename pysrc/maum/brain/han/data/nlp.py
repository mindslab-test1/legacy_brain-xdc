import typing
import grpc
import collections
import re
import logging

from functools import partial
from multiprocessing.pool import ThreadPool
from tqdm import tqdm

import maum.brain.han.data.feature as feature
import maum.brain.han.data.han_data_pb2 as data_pb2
import maum.brain.nlp.nlp_pb2 as brain_nlp_pb2
import maum.brain.nlp.nlp_pb2_grpc as nlp_pb2_grpc
import maum.common.lang_pb2 as lang_pb2

from maum.brain.han.data.han_dataset import HanData
from maum.brain.han.data.util import NlpUtil
from common.config import Config
from maum.brain.han.utils.general import tqdm_to_logger

logger = logging.getLogger('subproc')


class NlpBase(object):
    conf = Config()

    def __init__(self, feature_list: feature.FeatureList,
                 option: data_pb2.Nlp.Common) -> None:
        self.num_workers = int(self.conf.get('brain-han.nlp.num.worker'))

        self.word_feature = feature_list.register_discrete_feature(
            feature.FeatureLevel.WORD, option.word
        )

        self.pos_feature = feature_list.register_discrete_feature(
            feature.FeatureLevel.WORD, option.pos
        ) if option.use_pos else None

        self.ner_feature = feature_list.register_discrete_feature(
            feature.FeatureLevel.WORD, option.ner
        ) if option.use_ner else None

        self.char_feature = feature_list.register_discrete_feature(
            feature.FeatureLevel.CHAR, option.char
        ) if option.use_char else None

        self.tf_feature = feature_list.register_continuous_feature(
            feature.FeatureLevel.WORD, 1
        ) if option.use_tf else None

        self.support_lang_list = []

    def check_support_lang(self, lang: lang_pb2.LangCode):
        if lang not in self.support_lang_list:
            nlp_name = self.__class__.__name__
            lang_name = lang_pb2.LangCode.Name(lang)
            err_msg = "{} doesn't support {}".format(nlp_name, lang_name)
            raise RuntimeError(err_msg)

    def make_feature(self, han_data_list: typing.List[HanData],
                     evaluation: bool) -> None:
        tqdm_out = tqdm_to_logger(logger)
        pbar = tqdm(total=len(han_data_list), file=tqdm_out, mininterval=1)
        make_feature_one = partial(
            self.make_feature_one,
            evaluation=evaluation,
            pbar=pbar
        )

        pool = ThreadPool(processes=self.num_workers)
        try:
            pool.map(make_feature_one, han_data_list)
        except Exception as e:
            pool.terminate()
            raise e
        finally:
            pool.close()
            pool.join()
            pbar.close()

    def make_feature_one(self, han_data: HanData,
                          evaluation: bool, pbar: tqdm) -> None:
        word_data, morp_data, pos_data, ner_data, map_dic = self._analyze(han_data.text_list, evaluation)
        self._input_feature_data(han_data, evaluation,
                                 word_data, morp_data, pos_data, ner_data,
                                 map_dic)
        if pbar is not None:
            pbar.update(1)

    def parse_nlp_doc(self, han_data: HanData, sentence_list,
                      evaluation: bool) -> None:
        word_data, morp_data, pos_data, ner_data, map_dic = self._parse_nlp_doc(sentence_list, evaluation)
        self._input_feature_data(han_data, evaluation,
                                 word_data, morp_data, pos_data, ner_data,
                                 map_dic)

    def _input_feature_data(self, han_data: HanData, evaluation: bool,
                            word_data, morp_data, pos_data, ner_data, map_dic) -> None:
        feature_data = han_data.feature_data

        feature_data.input_size(word_data)
        feature_data.input_discrete_data(self.word_feature, morp_data)
        if self.pos_feature is not None:
            feature_data.input_discrete_data(self.pos_feature, pos_data)
        if self.ner_feature is not None:
            feature_data.input_discrete_data(self.ner_feature, ner_data)
        if self.char_feature is not None:
            feature_data.input_discrete_data(self.char_feature, word_data)
        if self.tf_feature is not None:
            tf_data = NlpUtil.make_tf_data(morp_data)
            feature_data.input_continuous_data(self.tf_feature, tf_data)

        if evaluation:
            han_data.map_dic = map_dic

    def _analyze(self, text_list: typing.List[str], evaluation: bool):
        raise NotImplementedError

    def _parse_nlp_doc(self, sentence_list, evaluation: bool, text_list=None):
        raise NotImplementedError


class BrainNlpEng2(NlpBase):
    def __init__(self, feature_list: feature.FeatureList,
                 option: data_pb2.Nlp) -> None:
        super(BrainNlpEng2, self).__init__(feature_list, option.common)

        self.double_quotes_pattern = re.compile('''“|”|''|"''')

        self.text_analysis_level = 2 if self.ner_feature is not None else 1
        self._make_stub()

        self.support_lang_list.append(lang_pb2.eng)

    def _make_stub(self):
        remote = self.conf.get('brain-han.nlp2.eng.remote')
        channel = grpc.insecure_channel(remote)
        self.stub = nlp_pb2_grpc.NaturalLanguageProcessingServiceStub(channel)

    def _make_in_text(self, text: str) -> brain_nlp_pb2.InputText:
        in_text = brain_nlp_pb2.InputText()
        in_text.text = text
        in_text.lang = lang_pb2.eng
        in_text.split_sentence = True
        in_text.use_tokenizer = False
        in_text.level = self.text_analysis_level
        return in_text

    def _analyze(self, text_list: typing.List[str], evaluation: bool):
        in_text_list = (self._make_in_text(text) for text in text_list)
        try:
            ret_list = self.stub.AnalyzeMultiple(in_text_list)
        except grpc.RpcError:
            self._make_stub()
            logger.warning('grpc timeout too short. reconnect grpc.')
            ret_list = self.stub.AnalyzeMultiple(in_text_list)

        sentence_list = [ret.sentences for ret in ret_list]

        return self._parse_nlp_doc(sentence_list, evaluation, text_list)

    def _parse_nlp_doc(self, sentence_list, evaluation: bool, text_list=None):
        word_matrix = [
            [word.text for sentence in sentences for word in sentence.words]
            for sentences in sentence_list
        ]
        morp_matrix = [
            [morp.lemma for sentence in sentences for morp in sentence.morps]
            for sentences in sentence_list
        ]

        pos_matrix = [
            [morp.type for sentence in sentences for morp in sentence.morps]
            for sentences in sentence_list
        ] if self.pos_feature is not None else None

        ner_matrix = None
        if self.ner_feature is not None:
            ner_matrix = []
            for sentences in sentence_list:
                ner_list = []
                for sentence in sentences:
                    ner_list_i = [''] * len(sentence.morps)
                    for ner in sentence.nes:
                        for index in range(ner.begin, ner.end + 1):
                            ner_list_i[index] = ner.type
                    ner_list.extend(ner_list_i)
                ner_matrix.append(ner_list)

        map_dic = self._make_map_dic(text_list,
                                     word_matrix) if evaluation else None

        return word_matrix, morp_matrix, pos_matrix, ner_matrix, map_dic

    def _make_map_dic(self, text_list: typing.List[str], word_matrix):
        map_dic = collections.defaultdict(lambda: collections.defaultdict(list))
        for s_idx, word_list in enumerate(word_matrix):
            text = text_list[s_idx]
            index_of_char = 0
            origin_text = re.sub("[‘’`']", "'", text)
            search_range = origin_text
            for w_idx, word in enumerate(word_list):
                if word in ('``', "''"):
                    quotes = self.double_quotes_pattern.search(search_range)
                    if quotes is None:
                        logger.warning('mapping error: {}\t{}'.format(
                            word, search_range))
                        start = index_of_char
                        end = index_of_char
                    else:
                        start = index_of_char + quotes.start()
                        end = index_of_char + quotes.end()
                else:
                    if word == 'nlp':
                        word = 'ta'
                    else:
                        word = re.sub("[‘’`']", "'", word)
                        word = re.sub(r'\\\\', r'\\', word)
                    start_index = search_range.find(word)
                    if start_index == -1:
                        logger.warning('mapping error: {}\t{}'.format(
                            word, search_range))
                        start = index_of_char
                        end = index_of_char
                    else:
                        start = index_of_char + start_index
                        end = start + len(word)

                map_dic[s_idx][w_idx] = start, end
                index_of_char = map_dic[s_idx][w_idx][1]
                search_range = origin_text[index_of_char:]
        return map_dic


class BrainNlpKor3(NlpBase):
    def __init__(self, feature_list: feature.FeatureList,
                 option: data_pb2.Nlp) -> None:
        super(BrainNlpKor3, self).__init__(feature_list, option.common)

        self.text_analysis_level = 2 if self.ner_feature is not None else 1
        self._make_stub()

        self.support_lang_list.append(lang_pb2.kor)

    def _make_stub(self):
        remote = self.conf.get('brain-han.nlp3.kor.remote')
        channel = grpc.insecure_channel(remote)
        self.stub = nlp_pb2_grpc.NaturalLanguageProcessingServiceStub(channel)

    def _make_in_text(self, text: str) -> brain_nlp_pb2.InputText:
        in_text = brain_nlp_pb2.InputText()
        in_text.text = text
        in_text.lang = lang_pb2.kor
        in_text.split_sentence = True
        in_text.use_tokenizer = False
        in_text.level = self.text_analysis_level
        return in_text

    def _analyze(self, text_list: typing.List[str], evaluation):
        in_text_list = (self._make_in_text(text) for text in text_list)
        try:
            ret_list = self.stub.AnalyzeMultiple(in_text_list)
        except grpc.RpcError:
            self._make_stub()
            logger.warning('grpc timeout too short. reconnect grpc.')
            ret_list = self.stub.AnalyzeMultiple(in_text_list)

        sentence_list = [ret.sentences for ret in ret_list]

        return self._parse_nlp_doc(sentence_list, evaluation)

    def _parse_nlp_doc(self, sentence_list, evaluation: bool, text_list=None):
        word_matrix = [
            ['{}/{}'.format(morp.lemma, morp.type) for sentence in sentences for morp in sentence.morps]
            for sentences in sentence_list
        ]
        morp_matrix = word_matrix

        pos_matrix = [
            [morp.type for sentence in sentences for morp in sentence.morps]
            for sentences in sentence_list
        ] if self.pos_feature is not None else None

        ner_matrix = None
        if self.ner_feature is not None:
            ner_matrix = []
            for sentences in sentence_list:
                ner_list = []
                for sentence in sentences:
                    ner_list_i = [''] * len(sentence.morps)
                    for ner in sentence.nes:
                        for index in range(ner.begin, ner.end + 1):
                            ner_list_i[index] = ner.type
                    ner_list.extend(ner_list_i)
                ner_matrix.append(ner_list)

        map_dic = self._make_map_dic([[word.text for sentence in sentences for word in sentence.words] for sentences in sentence_list],
                                     [[[morp.lemma for morp in sentence.morps[word.begin:word.end+1]] for sentence in sentences for word in sentence.words] for sentences in sentence_list]) if evaluation else None

        return word_matrix, morp_matrix, pos_matrix, ner_matrix, map_dic

    def _make_map_dic(self, words_list, morps_list):
        map_dic = collections.defaultdict(lambda: collections.defaultdict(list))
        for s_dix, (words, morps) in enumerate(zip(words_list, morps_list)):
            m_idx = 0
            o_idx = 0
            for o, m in zip(words, morps):
                max_o_idx = o_idx + len(o)
                if len(m) == 1:
                    map_dic[s_dix][m_idx] = (o_idx, o_idx + len(o))
                    m_idx += 1
                    o_idx += len(o) + 1
                else:
                    pass_list = list()  # splitted morph ex. 나서+었
                    start_from = 0
                    for each_m in m:
                        location = o.find(each_m, start_from)
                        if location >= 0:
                            map_dic[s_dix][m_idx] = (o_idx + location, o_idx + location + len(each_m))
                            start_from = location + len(each_m)
                        else:
                            pass_list.append((m_idx, m, start_from + o_idx, o))
                        m_idx += 1
                    pass_list_tmp = pass_list
                    if len(pass_list) > 0:  # if there is a splitted morphs
                        for pass_item in pass_list:
                            # m_idx, m, start_position
                            backward_until = 1
                            while m_idx > (pass_item[0] + backward_until):
                                try:
                                    # 만약 그 다음 것도 찾지 못하면 하나 더 뒤로 감
                                    map_dic[s_dix][pass_item[0]] = (pass_item[2], map_dic[s_dix][pass_item[0] + backward_until][0])
                                    pass_list_tmp.remove(pass_item)
                                    break
                                except IndexError:
                                    backward_until += 1
                    if len(pass_list_tmp) > 0:
                        for pass_item in pass_list_tmp:
                            map_dic[s_dix][pass_item[0]] = (pass_item[2], max_o_idx)
                    o_idx = max_o_idx
                    o_idx += 1  # space
        return map_dic


class SimpleNlp(NlpBase):
    def __init__(self, feature_list: feature.FeatureList,
                 option: data_pb2.Nlp) -> None:
        if option.common.use_pos:
            logger.warning('SimpleNlp can not use pos')
        option.common.use_pos = False
        if option.common.use_ner:
            logger.warning('SimpleNlp can not use ner')
        option.common.use_ner = False
        super(SimpleNlp, self).__init__(feature_list, option.common)
        self.not_space_pattern = re.compile(r'[^ ]+')

        self.support_lang_list.append(lang_pb2.kor)
        self.support_lang_list.append(lang_pb2.eng)

    def _analyze(self, text_list: typing.List[str], evaluation):
        word_matrix = [self.not_space_pattern.findall(text) for text in text_list]
        morp_matrix = word_matrix
        pos_matrix = None
        ner_matrix = None

        map_dic = self._make_map_dic(text_list,
                                     word_matrix) if evaluation else None

        return word_matrix, morp_matrix, pos_matrix, ner_matrix, map_dic

    def _make_map_dic(self, text_list: typing.List[str], word_matrix):
        map_dic = collections.defaultdict(lambda: collections.defaultdict(list))

        for s_idx, word_list in enumerate(word_matrix):
            search_range = text_list[s_idx]
            index_of_char = 0
            for w_idx, word in enumerate(word_list):
                start_index = search_range.find(word)
                if start_index == -1:
                    logger.warning('mapping error: {}\t{}'.format(
                        word, search_range))
                    start = index_of_char
                    end = index_of_char
                else:
                    start = index_of_char + start_index
                    end = start + len(word)
                map_dic[s_idx][w_idx] = start, end
        return map_dic

    def _parse_nlp_doc(self, sentence_list, evaluation: bool, text_list=None):
        raise RuntimeError("SimpleNlp doesn't support _parse_nlp_doc")
