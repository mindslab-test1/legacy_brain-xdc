import typing

from torch.utils.data import Dataset

from maum.brain.han.data.feature import FeatureDataList


class HanData(object):
    def __init__(self, text_list: typing.List[str], label: str, uid: str,
                 feature_data: FeatureDataList) -> None:
        self.text_list = text_list
        self.label = label
        self.uid = uid
        self.feature_data = feature_data
        self.map_dic = None


class HanDataset(Dataset):
    def __init__(self, data_list: typing.List[HanData]) -> None:
        self.data_list = data_list

    def __len__(self):
        return len(self.data_list)

    def __getitem__(self, index):
        return self.data_list[index]
