import os
import typing
import re
import pandas as pd

import maum.brain.han.data.feature as feature

from maum.brain.han.data.han_dataset import HanData


class RawDataLoaderBase(object):
    def __init__(self, feature_list: feature.FeatureList,
                 label_list: typing.List[str],
                 extra_features: typing.Dict[str, feature.Feature]) -> None:
        self.feature_list = feature_list

        self.label_list = label_list
        self.label_dict = {
            item: index for index, item in enumerate(self.label_list)
        }

        self.extra_features = extra_features


class AclimdbRawDataLoader(RawDataLoaderBase):
    def __init__(self, feature_list: feature.FeatureList,
                 label_list: typing.List[str],
                 extra_features: typing.Dict[str, feature.Feature]) -> None:
        super(AclimdbRawDataLoader, self).__init__(feature_list,
                                                   label_list,
                                                   extra_features)

    def load_raw_data(self, encoding: str, dir_path: str,
                      data_name: str) -> typing.List[HanData]:
        data_list = []

        for label in self.label_list:
            data_path = os.path.join(dir_path, label)

            file_name_list = os.listdir(data_path)
            file_name_list.sort()

            for file_name in file_name_list:
                file_path = os.path.join(data_path, file_name)
                with open(file_path, 'r', encoding=encoding) as file:
                    text = file.read()
                text = re.sub('([^0-9][.!?])([^ ])', r'\1 \2', text)
                text = re.sub('([.!?]+)[ ]+', r'\1\n', text)
                text = text.strip()
                text_list = text.split('\n')

                order = file_name[:file_name.find('_')]
                uid = '_'.join([data_name, label, order])

                feature_data = self.feature_list.make_feature_data_list(
                    self.label_dict[label])

                han_data = HanData(text_list, label, uid, feature_data)
                data_list.append(han_data)

        return data_list


class TaRawDataLoader(RawDataLoaderBase):
    def __init__(self, feature_list: feature.FeatureList,
                 label_list: typing.List[str],
                 extra_features: typing.Dict[str, feature.Feature]) -> None:
        super(TaRawDataLoader, self).__init__(feature_list,
                                              label_list,
                                              extra_features)

    def load_raw_data(self, encoding: str,
                      file_path: str) -> typing.List[HanData]:

        data_list = []

        with open(file_path, 'r', encoding=encoding) as file:
            uid = 0
            for line in file.readlines():
                uid += 1

                line_split = line.split('\t')
                text = line_split[0]
                text = re.sub('([^0-9][.!?])([^ ])', r'\1 \2', text)
                text = re.sub('([.!?]+)[ ]+', r'\1\n', text)
                text = text.strip()
                text_list = text.split('\n')

                label = line_split[1].split('\n')[0]

                feature_data = self.feature_list.make_feature_data_list(
                    self.label_dict[label])
                han_data = HanData(text_list, label, uid, feature_data)
                data_list.append(han_data)

        return data_list


class TsvRawDataLoader(RawDataLoaderBase):
    def __init__(self, feature_list: feature.FeatureList,
                 label_list: typing.List[str],
                 extra_features: typing.Dict[str, feature.Feature]) -> None:
        super(TsvRawDataLoader, self).__init__(feature_list,
                                               label_list,
                                               extra_features)

    def load_raw_data(self, encoding: str,
                      col_name_list: typing.List[str],
                      doc_uid_name: str,
                      sentence_name: str,
                      data_file_path: str,
                      label_file_path: str) -> typing.List[HanData]:
        data_list = []

        with open(label_file_path, 'r', encoding=encoding) as f:
            label_dict = {
                line.split(' ')[0]: line.split(' ')[1].split('\n')[0]
                for line in f.readlines()
            }

        raw_data = pd.read_csv(data_file_path, sep='\t', encoding=encoding, names=col_name_list)
        doc_feature_data = raw_data[[doc_uid_name]]
        doc_feature_data = doc_feature_data.drop_duplicates()

        for row in doc_feature_data.itertuples():
            uid = row[1]
            label = label_dict[uid]

            if label in self.label_list:
                sentence_data = raw_data[raw_data[doc_uid_name].values == uid]

                text_list = sentence_data[sentence_name].tolist()

                feature_data = self.feature_list.make_feature_data_list(
                    self.label_dict[label])
                for col_name, extra_feature in self.extra_features.items():
                    extra_data = sentence_data[col_name].tolist()
                    if extra_feature.level == feature.FeatureLevel.DOC.value:
                        extra_data = extra_data[0]

                    if type(extra_feature) == feature.ContinuousFeature:
                        if extra_feature.level == feature.FeatureLevel.SENTENCE.value:
                            extra_data = [[data] for data in extra_data]
                        elif extra_feature.level == feature.FeatureLevel.DOC.value:
                            extra_data = [extra_data]
                        feature_data.input_continuous_data(extra_feature,
                                                           extra_data)
                    elif type(extra_feature) == feature.DiscreteFeature:
                        feature_data.input_discrete_data(extra_feature,
                                                         extra_data)

                han_data = HanData(text_list, label, uid, feature_data)
                data_list.append(han_data)

        return data_list
