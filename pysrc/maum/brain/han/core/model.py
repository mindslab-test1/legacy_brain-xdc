import logging
import torch
import torch.nn as nn
import torch.nn.functional as F

from torch.autograd import Variable

import maum.brain.han.train.han_train_pb2 as train_pb2
import maum.brain.han.core.han_core_pb2 as core_pb2
import maum.brain.han.core.util as util

from maum.brain.han.core.module.classifier import HanClassifier
from maum.brain.han.data.feature import FeatureList
from common.config import Config

logger = logging.getLogger('subproc')


class ClassifierModel(object):
    def __init__(self, label_num: int, feature_list: FeatureList,
                 option: core_pb2.Model, state_dict=None, device=None) -> None:
        self.grad_clipping = option.grad_clipping
        self.updates = state_dict['updates'] if state_dict else 0
        self.use_gpu = False
        self.parallel = False
        self.device = device

        self.train_loss = util.AverageMeter()
        self.test_loss = util.AverageMeter()

        self.network = HanClassifier(label_num, feature_list,
                                     option.classifier)
        if state_dict:
            new_state = set(self.network.state_dict().keys())
            for k in list(state_dict['network'].keys()):
                if k not in new_state:
                    del state_dict['network'][k]
            self.network.load_state_dict(state_dict['network'])

        for embedding_level in self.network.embedding:
            for embedding_level_i in embedding_level:
                embedding_level_i.register_parameters()

        parameters = [p for p in self.network.parameters() if p.requires_grad]
        self.optimizer = util.OptimizerFactory.make(parameters,
                                                    option.optimizer)
        if state_dict:
            self.optimizer.load_state_dict(state_dict['optimizer'])

        conf = Config()
        use_gpu = conf.get('brain-han.trainer.use.gpu') == 'true'
        if torch.cuda.is_available() and use_gpu:
            self.use_gpu = True
            use_multi_gpu = conf.get('brain-han.trainer.use.multi.gpu') == 'true'
            if use_multi_gpu and torch.cuda.device_count() > 1 and device is None:
                self.network = nn.parallel.DataParallel(self.network)
                self.parallel = True
            self.network.cuda(device)

    def update(self, input_list):
        self.network.train()

        if self.use_gpu:
            inputs = [
                e if e is None else Variable(e.cuda(device=self.device, async=True))
                for e in input_list[:12]
            ]
            target = Variable(input_list[-2].cuda(device=self.device, async=True))
        else:
            inputs = [
                e if e is None else Variable(e)
                for e in input_list[:12]
            ]
            target = Variable(input_list[-2])

        scores, _ = self.network(*inputs)

        loss = F.cross_entropy(scores, target)
        self.train_loss.update(loss.data[0], target.size(0))

        self.optimizer.zero_grad()
        loss.backward()

        torch.nn.utils.clip_grad_norm(self.network.parameters(),
                                      self.grad_clipping)

        self.optimizer.step()
        self.updates += 1

        self.reset_parameters()

        scores_data = scores.data

        _, prediction_tensor = torch.max(scores_data, 1)
        correct_tensor = torch.eq(prediction_tensor, target.data)

        correct = torch.sum(correct_tensor)
        return correct

    def test(self, input_list):
        self.network.eval()

        if self.use_gpu:
            inputs = [
                e if e is None
                else Variable(e.cuda(device=self.device, async=True), volatile=True)
                for e in input_list[:12]
            ]
            target = Variable(input_list[-2].cuda(device=self.device, async=True), volatile=True)
        else:
            inputs = [
                e if e is None else Variable(e, volatile=True)
                for e in input_list[:12]
            ]
            target = Variable(input_list[-2], volatile=True)

        scores, att_result_list = self.network(*inputs)

        loss = F.cross_entropy(scores, target)
        self.test_loss.update(loss.data[0], target.size(0))

        scores_norm = F.softmax(scores, -1)
        scores_norm_data = scores_norm.data
        score_tensor, prediction_tensor = torch.max(scores_norm_data, 1)

        correct_tensor = torch.eq(prediction_tensor, target.data)
        correct = torch.sum(correct_tensor)

        att_result_list = [
            None
            if att_result is None else att_result.data.view(mask.data.size())
            for att_result, mask in zip(att_result_list, inputs[8:12])
        ]

        return correct, (prediction_tensor, score_tensor, att_result_list)

    def predict(self, input_list, top_n=1, use_attn_output=False):
        self.network.eval()

        if self.use_gpu:
            inputs = [
                e if e is None
                else Variable(e.cuda(device=self.device, async=True), volatile=True)
                for e in input_list[:12]
            ]
        else:
            inputs = [
                e if e is None else Variable(e, volatile=True)
                for e in input_list[:12]
            ]

        scores, att_result_list = self.network(*inputs)
        scores_norm = F.softmax(scores, -1)

        scores_norm_data = scores_norm.data

        score_tensor, prediction_tensor = torch.topk(scores_norm_data, top_n,
                                                     dim=1, largest=True,
                                                     sorted=True)

        att_result_list = [
            None
            if att_result is None else att_result.data.view(mask.data.size())
            for att_result, mask in zip(att_result_list, inputs[8:12])
        ] if use_attn_output else None
        return prediction_tensor, score_tensor, att_result_list

    def reset_parameters(self):
        if self.parallel:
            embedding = self.network.module.embedding
        else:
            embedding = self.network.embedding
        for embedding_level in embedding:
            for embedding_level_i in embedding_level:
                embedding_level_i.reset_parameters()

    def save(self, filename: str, epoch: int, option: train_pb2.Trainer):
        if self.parallel:
            network = self.network.module
        else:
            network = self.network
        params = {
            'state_dict': {
                'network': network.state_dict(),
                'optimizer': self.optimizer.state_dict(),
                'updates': self.updates
            },
            'option': option,
            'epoch': epoch
        }
        try:
            torch.save(params, filename)
            logger.info('model saved to {}'.format(filename))
        except BaseException:
            logger.warning('[ WARN: Saving failed... continuing anyway. ]')
