import torch.optim as optim

import maum.brain.han.core.han_core_pb2 as core_pb2


class AverageMeter(object):
    """Computes and stores the average and current value."""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


class OptimizerFactory(object):
    @staticmethod
    def make(parameters,
             option: core_pb2.Optimizer) -> optim.Optimizer:
        if option.optimizer_type == core_pb2.ADAMAX:
            admax_opt = option.admax
            return optim.Adamax(parameters, admax_opt.lr,
                                (admax_opt.beta1, admax_opt.beta2),
                                admax_opt.eps, admax_opt.weight_decay)
        elif option.optimizer_type == core_pb2.SGD:
            sgd_opt = option.sgd
            return optim.SGD(parameters, sgd_opt.lr, sgd_opt.momentum,
                             sgd_opt.dampening, sgd_opt.weight_decay,
                             sgd_opt.nesterov)
        else:
            raise RuntimeError('wrong OptimizerType')
