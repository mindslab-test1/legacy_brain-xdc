import torch
import torch.nn as nn
import torch.nn.functional as F

from maum.brain.han.core.module.attention import LinearSeqAttn
from maum.brain.han.core.module.cuda_functional import SRUCell


class StackedBRNN(nn.Module):
    """Stacked Bi-directional RNNs.
    Differs from standard PyTorch library in that it has the option to save
    and concat the hidden states between layers. (i.e. the output hidden size
    for each sequence input is num_layers * hidden_size).
    """
    def __init__(self, input_size, hidden_size, num_layers,
                 dropout_rate=0, dropout_output=False, rnn_type=nn.LSTM,
                 concat_layers=False, sum_layers=False, att_layers=False):
        super(StackedBRNN, self).__init__()
        self.dropout_output = dropout_output
        self.dropout_rate = dropout_rate
        self.num_layers = num_layers
        self.concat_layers = concat_layers
        self.sum_layers = sum_layers
        self.self_att = LinearSeqAttn(2 * hidden_size) if att_layers else None
        self.rnns = nn.ModuleList()
        for i in range(num_layers):
            input_size = input_size if i == 0 else 2 * hidden_size
            if rnn_type == SRUCell:
                rnn = SRUCell(input_size, hidden_size,
                              dropout=dropout_rate,
                              rnn_dropout=dropout_rate,
                              bidirectional=True)
            else:
                rnn = rnn_type(input_size, hidden_size, num_layers=1,
                               bidirectional=True)
            self.rnns.append(rnn)

        self.output_size = 2 * hidden_size
        if self.concat_layers:
            self.output_size *= num_layers

    def forward(self, x, x_mask):
        # Transpose batch and sequence dims
        x = x.transpose(0, 1)

        # Encode all layers
        outputs = [x]
        for i in range(self.num_layers):
            rnn_input = outputs[-1]

            # Apply dropout to hidden input
            if type(self.rnns[i]) != SRUCell and self.dropout_rate > 0:
                rnn_input = F.dropout(rnn_input,
                                      p=self.dropout_rate,
                                      training=self.training)
            # Forward
            if type(self.rnns[i]) != SRUCell:
                self.rnns[i].flatten_parameters()
            rnn_output = self.rnns[i](rnn_input)[0]

            outputs.append(rnn_output)

        # Concat hidden layers
        if self.concat_layers is True:
            output = torch.cat(outputs[1:], 2)
        elif self.sum_layers is True and self.num_layers > 1:
            output = outputs[1] + outputs[-1]
        elif self.self_att:
            hiddens = torch.stack(outputs[1:], 2)
            hiddens_flat = hiddens.view(-1, hiddens.size(-2), hiddens.size(-1))

            x_mask_flat = x_mask.transpose(0, 1).unsqueeze(2).contiguous()
            x_mask_flat = x_mask_flat.view(-1, 1).expand(hiddens_flat.size()[:2])

            merge_weights = self.self_att(hiddens_flat, x_mask_flat)
            output_flat = merge_weights.unsqueeze(1).bmm(hiddens_flat).squeeze(1)
            output = output_flat.view(x_mask.size(1), x_mask.size(0), -1)
        else:
            output = outputs[-1]

        # Transpose back
        output = output.transpose(0, 1)

         # Dropout on output layer
        if self.dropout_output is True and self.dropout_rate > 0:
            output = F.dropout(output,
                               p=self.dropout_rate,
                               training=self.training)

        return output
