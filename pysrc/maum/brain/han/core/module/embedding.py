import torch
import torch.nn as nn
import torch.nn.functional as F

from maum.brain.han.data.feature import DiscreteFeature


class HanEmbedding(nn.Module):
    def __init__(self, discrete_feature: DiscreteFeature):
        super(HanEmbedding, self).__init__()

        self.dropout = 0
        self.offset = 0
        voca_dim = len(discrete_feature.dictionary)

        if discrete_feature.use_emb:
            emb_opt = discrete_feature.emb_opt
            self.dropout = emb_opt.dropout
            self.embedding = nn.Embedding(voca_dim, emb_opt.dim,
                                          padding_idx=0)
            if emb_opt.pre_train_file:
                if discrete_feature.emb_value is not None:
                    self.embedding.weight.data = discrete_feature.emb_value
                if emb_opt.tune_partial > 0:
                    self.offset = emb_opt.tune_partial + 2
                    self.fixed_embedding = None
                else:
                    for p in self.embedding.parameters():
                        p.requires_grad = False
        else:
            self.embedding = nn.Embedding(voca_dim, voca_dim, padding_idx=0)
            self.embedding.weight.data = torch.eye(voca_dim)
            for p in self.embedding.parameters():
                p.requires_grad = False

    def forward(self, x):
        x_emb = self.embedding(x)
        if self.dropout > 0:
            x_emb = F.dropout(x_emb, p=self.dropout, training=self.training)
        return x_emb

    def register_parameters(self):
        if self.offset > 0:
            fixed_embedding = self.embedding.weight.data[self.offset:]
            self.register_buffer('fixed_embedding', fixed_embedding)
            self.fixed_embedding = fixed_embedding

    def reset_parameters(self):
        if self.offset > 0:
            self.embedding.weight.data[self.offset:] = self.fixed_embedding
