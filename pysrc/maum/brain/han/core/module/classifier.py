import torch
import torch.nn as nn

import maum.brain.han.data.feature as feature
import maum.brain.han.core.han_core_pb2 as core_pb2

from maum.brain.han.core.module.embedding import HanEmbedding
from maum.brain.han.core.module.encoder import Ffnn
from maum.brain.han.core.module.util import EncoderFactory


class HanClassifier(nn.Module):
    def __init__(self, label_num: int, feature_list: feature.FeatureList,
                 option: core_pb2.Classifier):
        super(HanClassifier, self).__init__()

        self.embedding = nn.ModuleList([
            nn.ModuleList([
                HanEmbedding(discrete_feature) for discrete_feature in features
            ]) for features in feature_list.discrete_feature_list
        ])

        input_size_list = [0] * feature.FeatureLevel.__len__()
        for level in feature.FeatureLevel.get_list_asc():
            for discrete_feature in feature_list.discrete_feature_list[level]:
                if discrete_feature.use_emb:
                    input_size_list[level] += discrete_feature.emb_opt.dim
                else:
                    input_size_list[level] += len(discrete_feature.dictionary)
            for continuous_feature in feature_list.continuous_feature_list[level]:
                input_size_list[level] += continuous_feature.dim

        self.encoder = nn.ModuleList()

        encoder_opt_list = [option.char_encoder,
                            option.word_encoder,
                            option.sentence_encoder]
        ouput_size = 0
        for level, encoder_opt in enumerate(encoder_opt_list):
            input_size = ouput_size + input_size_list[level]
            encoder = EncoderFactory.make(input_size, encoder_opt)
            feature_list.init_max_size[level] = encoder.min_seq_size
            ouput_size = encoder.output_size
            self.encoder.append(encoder)
        feature_list.init_max_size.reverse()

        input_size = ouput_size + input_size_list[feature.FeatureLevel.DOC.value]
        doc_encoder = Ffnn(input_size, label_num, option.doc_encoder)
        self.encoder.append(doc_encoder)

    def forward(self, *inputs):
        discrete_data_list = [
            data.view(-1, data.size(-2), data.size(-1))
            if data is not None else data
            for data in inputs[0:4]
        ]
        continuous_data_list = [
            data.view(-1, data.size(-2), data.size(-1))
            if data is not None else data
            for data in inputs[4:8]
        ]
        mask_list = [data.view(-1, data.size(-1)) for data in inputs[8:12]]

        hidden = None
        att_result = None
        att_result_list = []
        for level in feature.FeatureLevel.get_list_asc():
            encoder_input_list = []
            if hidden is not None:
                hidden_flat = hidden.view(mask_list[level].size(0),
                                          mask_list[level].size(1), -1)
                encoder_input_list.append(hidden_flat)

            if discrete_data_list[level] is not None:
                discrete_data = torch.split(discrete_data_list[level], 1, 2)
                for embedding, data in zip(self.embedding[level], discrete_data):
                    data_flat = torch.squeeze(data, 2)
                    data_emb = embedding(data_flat)
                    encoder_input_list.append(data_emb)

            if continuous_data_list[level] is not None:
                encoder_input_list.append(continuous_data_list[level])

            if len(encoder_input_list) > 0:
                encoder_input = torch.cat(encoder_input_list, 2)
                hidden, att_result = self.encoder[level](encoder_input,
                                                         mask_list[level])
            att_result_list.append(att_result)
        return hidden, att_result_list
