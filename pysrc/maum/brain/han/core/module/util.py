import torch

import maum.brain.han.core.han_core_pb2 as core_pb2
import maum.brain.han.core.module.encoder as han_encoder


class EncoderFactory(object):
    @staticmethod
    def make(input_size: int, encoder_opt) -> torch.nn.Module:
        if type(encoder_opt) == core_pb2.Cnn:
            return han_encoder.CnnText(input_size, encoder_opt)
        elif type(encoder_opt) == core_pb2.RnnAtt:
            return han_encoder.RnnAttentionEncoder(input_size, encoder_opt)
        else:
            raise RuntimeError('wrong EncoderType')
