import torch
import torch.nn as nn
import torch.nn.functional as F

import maum.brain.han.core.han_core_pb2 as core_pb2

from maum.brain.han.core.module.cuda_functional import SRUCell
from maum.brain.han.core.module.attention import LinearSeqAttn
from maum.brain.han.core.module.nn import StackedBRNN


class RnnAttentionEncoder(nn.Module):
    RNN_TYPES = {
        core_pb2.RNN: nn.RNN,
        core_pb2.LSTM: nn.LSTM,
        core_pb2.GRU: nn.GRU,
        core_pb2.SRU: SRUCell
    }

    def __init__(self, input_size: int, option: core_pb2.RnnAtt):
        super(RnnAttentionEncoder, self).__init__()

        self.rnn = StackedBRNN(
            input_size,
            hidden_size=option.hidden_size,
            num_layers=option.num_layers,
            dropout_rate=option.dropout_rate,
            dropout_output=option.dropout_output,
            rnn_type=self.RNN_TYPES[option.rnn_type],
            concat_layers=option.output_type == core_pb2.CONCAT,
            sum_layers=option.output_type == core_pb2.SUM,
            att_layers=option.output_type == core_pb2.ATT
        )
        self.self_att = LinearSeqAttn(self.rnn.output_size)
        self.output_size = self.rnn.output_size
        self.min_seq_size = 0

    def forward(self, x, x_mask):
        hiddens = self.rnn(x, x_mask)
        merge_weights = self.self_att(hiddens, x_mask)
        hidden = merge_weights.unsqueeze(1).bmm(hiddens).squeeze(1)
        return hidden, merge_weights


class CnnText(nn.Module):
    """A CNN model for text"""
    def __init__(self, input_size, option: core_pb2.Cnn):
        super(CnnText, self).__init__()
        ci = 1
        self.convs1 = nn.ModuleList(
            [
                nn.Conv2d(ci, option.num_filters, (k, input_size))
                for k in option.filter_sizes
            ]
        )
        self.dropout = nn.Dropout(option.dropout_rate)

        in_features = len(option.filter_sizes) * option.num_filters
        self.fc1 = nn.Linear(in_features, option.output_size)

        self.output_size = option.output_size
        self.min_seq_size = max(option.filter_sizes)

    def forward(self, x, x_mask):
        """
        input x: (batch, len_x, input_size)
        return : (batch, output_size)
        """
        # expand x to [batch, 1, len_x(H), input_size]
        x = x.unsqueeze(1)
        # (batch, 1, len_x, input_size) -> (batch, Co, H, 1) -> (batch, Co, H)
        # [(batch, Co, H), ...] * len(filter_sizes)
        x = [F.relu(conv(x)).squeeze(3) for conv in self.convs1]
        # [(batch, Co), ...] * len(filter_sizes)
        x = [F.max_pool1d(i, i.size(2)).squeeze(2) for i in x]
        # (batch, len(filter_sizes) * Co)
        x = torch.cat(x, 1)
        x = self.dropout(x)
        # (batch, output_size)
        logit = self.fc1(x)
        return logit, None


class Ffnn(nn.Module):
    def __init__(self, input_size: int, label_num: int, option: core_pb2.Ffnn):
        super(Ffnn, self).__init__()
        in_features = input_size
        module_list = []
        for layer_idx in range(option.num_layers):
            module_list.append(nn.Linear(in_features, option.hidden_size))
            if option.dropout_rate > 0:
                module_list.append(nn.Dropout(option.dropout_rate))
            if option.activation_type == core_pb2.RELU:
                module_list.append(nn.ReLU())
            elif option.activation_type == core_pb2.SIGMOID:
                module_list.append(nn.Sigmoid())
            elif option.activation_type == core_pb2.TANH:
                module_list.append(nn.Tanh())
            elif option.activation_type == core_pb2.ELU:
                module_list.append(nn.ELU())
            elif option.activation_type == core_pb2.SELU:
                module_list.append(nn.SELU())
            else:
                RuntimeError('wrong ActivationType')
            in_features = option.hidden_size

        module_list.append(nn.Linear(in_features, label_num))

        self.module_list = nn.Sequential(*module_list)

    def forward(self, x, x_mask):
        x_flat = x.view(-1, x.size(-1))
        scores = self.module_list(x_flat)
        return scores, None
