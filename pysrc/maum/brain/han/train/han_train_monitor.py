#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import grpc
from google.protobuf import empty_pb2 as _empty

import maum.brain.han.train.han_train_pb2_grpc as train_pb2_grpc


class HanTrainerMonitor(train_pb2_grpc.TrainerMonitorServicer):
    han_trainer = None
    """
    Provide methods that implement HanClassifier
    """
    def __init__(self, han_trainer):
        self.han_trainer = han_trainer

    def Notify(self, proc_stat, context):
        print('remote peer', context.peer())
        if 'ipv4:127.0.0.1' not in context.peer():
            context.set_code(grpc.StatusCode.PERMISSION_DENIED)
            context.set_details('Local internal use only')
            raise grpc.RpcError("permission denied")
        self.han_trainer.update_proc(proc_stat)
        return _empty.Empty()
