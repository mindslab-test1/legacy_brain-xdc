import matplotlib
matplotlib.use("Agg")
import matplotlib.pylab as plt
import os
import typing
import random

from torch.utils.data import DataLoader

import maum.brain.han.data.han_dataset as han_dataset
import maum.brain.han.data.feature as feature
import maum.brain.han.data.nlp as nlp
import maum.brain.han.data.raw_data_loader as loader
import maum.brain.han.utils.batch as batch
import maum.brain.han.data.han_data_pb2 as data_pb2
import maum.brain.han.train.han_train_pb2 as train_pb2
import maum.common.lang_pb2 as lang_pb2


def convert_test_data(sample_batched, result):
    uid_list = sample_batched[12]
    sentences_list = sample_batched[13]
    map_dic_list = sample_batched[14]
    target_list = sample_batched[-2].tolist()

    prediction_tensor, score_tensor, att_result_list = result

    prediction_list = prediction_tensor.cpu().tolist()
    score_list = score_tensor.cpu().tolist()
    sent_att_batch = att_result_list[2].cpu().tolist()
    word_atts_batch = att_result_list[1].cpu().tolist()

    return zip(uid_list, sentences_list, map_dic_list, target_list,
               prediction_list, score_list, sent_att_batch, word_atts_batch)


def make_test_file(result, label_list, test_result_home):
    header_list = []
    for uid, sentences, map_dic, target, prediction, score, sent_att_list, word_atts_list in result:
        target = label_list[target]
        prediction = label_list[prediction]

        header = '{}\t{}\t{}\t{}\n'.format(uid, target, score, prediction)
        header_list.append(header)

        test_result_file = os.path.join(test_result_home, '{}.txt'.format(uid))
        with open(test_result_file, 'w') as rf:
            for s_idx, word_dic in map_dic.items():
                sentence = sentences[s_idx]
                sent_att = sent_att_list[s_idx]
                word_atts = word_atts_list[s_idx]
                for w_idx, (start_idx, end_idx) in word_dic.items():
                    word = sentence[start_idx:end_idx]
                    word_att = word_atts[w_idx]
                    if word == '"':
                        word = '``'

                    rf.write('{}\t{}\t{}\t{}\t{}\n'.format(
                        s_idx, w_idx, word, sent_att, word_att))
    return header_list


class HanDataLoaderFactory(object):
    def __init__(self, use_gpu: bool,
                 feature_list: feature.FeatureList) -> None:
        self.use_gpu = use_gpu
        self.batch_generator = batch.BatchGenerator(feature_list)

    def make(self, dataset: han_dataset.HanDataset, max_batch_size: int,
             max_size_prod: int, is_shuffle: bool):
        han_data_loader = HanDataLoader(self.use_gpu, self.batch_generator,
                                        dataset, max_batch_size, max_size_prod,
                                        is_shuffle)
        return han_data_loader


class HanDataLoader(object):
    def __init__(self, use_gpu: bool, batch_generator: batch.BatchGenerator,
                 dataset: han_dataset.HanDataset,
                 max_batch_size: int, max_size_prod: int, is_shuffle: bool):
        self.use_gpu = use_gpu
        self.batch_generator = batch_generator
        self.dataset = dataset
        self.max_batch_size = max_batch_size
        self.max_size_prod = max_size_prod
        self.is_shuffle = is_shuffle
        self.data_loader = None
        self.make()

    def make(self):
        sampler = batch.SortedBatchSampler(self.dataset.data_list,
                                           self.max_size_prod,
                                           self.max_batch_size,
                                           self.is_shuffle)

        self.data_loader = DataLoader(
            self.dataset,
            batch_sampler=sampler,
            collate_fn=self.batch_generator.collate_fn,
            pin_memory=self.use_gpu
        )

    def __iter__(self):
        for data in self.data_loader:
            yield data

    def __len__(self):
        return len(self.data_loader)


class RawDataLoader(object):
    def __init__(self, data_home: str, feature_list: feature.FeatureList,
                 option: data_pb2.RawDataLoader):
        self.data_home = data_home
        extra_feature_list = option.extra_features if option.use_extra_feature else []

        extra_features = {}
        for extra_feature in extra_feature_list:
            name = extra_feature.name
            if extra_feature.level == data_pb2.SENTENCE:
                level = feature.FeatureLevel.SENTENCE
            elif extra_feature.level == data_pb2.DOC:
                level = feature.FeatureLevel.DOC
            else:
                raise RuntimeError('wrong ExtraFeature.level')

            if extra_feature.is_discrete:
                extra_features[name] = feature_list.register_discrete_feature(level, extra_feature.discrete)
            else:
                extra_features[name] = feature_list.register_continuous_feature(level, 1)

        self.label_list = option.labels
        self.aclimdb_raw_data_loader = loader.AclimdbRawDataLoader(
            feature_list, self.label_list, extra_features)
        self.ta_raw_data_loader = loader.TaRawDataLoader(
            feature_list, self.label_list, extra_features)
        self.tsv_raw_data_loader = loader.TsvRawDataLoader(
            feature_list, self.label_list, extra_features)

    def load_raw_data(self, data_opt: train_pb2.TrainData):
        encoding = data_opt.common.encoding
        if data_opt.raw_data_type == train_pb2.ACLIMDB:
            train_path = os.path.join(self.data_home,
                                      data_opt.aclimdb.dir_path,
                                      data_opt.aclimdb.train_dir)
            train_data_list = self.aclimdb_raw_data_loader.load_raw_data(
                encoding, train_path, 'train')

            test_path = os.path.join(self.data_home,
                                     data_opt.aclimdb.dir_path,
                                     data_opt.aclimdb.test_dir)
            test_data_list = self.aclimdb_raw_data_loader.load_raw_data(
                encoding, test_path, 'test')
        elif data_opt.raw_data_type == train_pb2.TA:
            file_path = os.path.join(self.data_home, data_opt.ta.file_path)
            data_list = self.ta_raw_data_loader.load_raw_data(
                encoding, file_path)
            train_data_list, test_data_list = self._split_data_list(
                data_list, data_opt.ta.test_ratio)
        elif data_opt.raw_data_type == train_pb2.TSV:
            data_file_path = os.path.join(self.data_home,
                                          data_opt.tsv.data_file_path)
            label_file_path = os.path.join(self.data_home,
                                           data_opt.tsv.label_file_path)
            data_list = self.tsv_raw_data_loader.load_raw_data(
                encoding,
                data_opt.tsv.col_names,
                data_opt.tsv.doc_uid_name,
                data_opt.tsv.sentence_name,
                data_file_path,
                label_file_path,
            )
            train_data_list, test_data_list = self._split_data_list(
                data_list, data_opt.tsv.test_ratio)
        else:
            raise RuntimeError('wrong RawDataType')
        return train_data_list, test_data_list

    def _split_data_list(self, data_list: typing.List[han_dataset.HanData],
                         test_ratio: int):
        han_data_list_dict = {
            label: [data for data in data_list if data.label == label]
            for label in self.label_list
        }

        train_data_list = []
        test_data_list = []
        for han_data_list in han_data_list_dict.values():
            random.shuffle(han_data_list)
            split_length = int(len(han_data_list) * test_ratio)

            train_data_list.extend(han_data_list[split_length:])
            test_data_list.extend(han_data_list[:split_length])
        return train_data_list, test_data_list


class NlpFactory(object):
    @staticmethod
    def make(feature_list: feature.FeatureList,
             option: data_pb2.Nlp, lang: lang_pb2.LangCode) -> nlp.NlpBase:
        if option.nlp_type == data_pb2.BRAIN_NLP_ENG2:
            nlp_impl = nlp.BrainNlpEng2(feature_list, option)
        elif option.nlp_type == data_pb2.SIMPLE_NLP:
            nlp_impl = nlp.SimpleNlp(feature_list, option)
        elif option.nlp_type == data_pb2.BRAIN_NLP_KOR3:
            nlp_impl = nlp.BrainNlpKor3(feature_list, option)
        else:
            raise RuntimeError('wrong NlpType')
        nlp_impl.check_support_lang(lang)
        return nlp_impl


class GraphMaker(object):
    def __init__(self, file_path: str):
        self.x_axis = []
        self.train_loss_list = []
        self.train_acc_list = []
        self.test_loss_list = []
        self.test_acc_list = []
        self.file_path = file_path

    def make(self, epoch: int, train_loss: float, train_acc: float,
             test_loss: float, test_acc: float):
        self.x_axis.append(epoch)
        self.train_loss_list.append(train_loss)
        self.train_acc_list.append(train_acc)
        self.test_loss_list.append(test_loss)
        self.test_acc_list.append(test_acc)

        plt.subplot(211)
        plt.title('loss')
        plt.xlabel('epoch')
        plt.ylabel('loss')
        plt.plot(self.x_axis, self.test_loss_list, label='test')
        plt.plot(self.x_axis, self.train_loss_list, label='train')
        plt.legend()

        plt.subplots_adjust(wspace=0.5, hspace=0.5)
        plt.grid(True)

        plt.subplot(212)
        plt.title('accuracy')
        plt.xlabel('epoch')
        plt.ylabel('acc')
        plt.plot(self.x_axis, self.test_acc_list, label='test')
        plt.plot(self.x_axis, self.train_acc_list, label='train')
        plt.legend()

        plt.subplots_adjust(wspace=0.5, hspace=0.5)
        plt.grid(True)

        fig = plt.gcf()
        fig.savefig(self.file_path)

        plt.clf()
