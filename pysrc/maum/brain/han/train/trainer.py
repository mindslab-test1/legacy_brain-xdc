import gc
import os
import torch
import time
import logging

from shutil import copyfile
from datetime import datetime
from functools import partial
from multiprocessing import Pool

import maum.common.lang_pb2 as lang_pb2
import maum.brain.han.train.han_train_pb2 as train_pb2
import maum.brain.han.train.util as util

from maum.brain.han.core.model import ClassifierModel
from maum.brain.han.data.han_dataset import HanDataset
from maum.brain.han.data.feature import FeatureList
from maum.brain.han.train.han_trainer_helper import TrainerHelper
from maum.brain.han.utils.error_handler import CudaErrorHandler
from common.config import Config

logger = logging.getLogger('subproc')


class Trainer(object):
    conf = Config()

    def __init__(self, helper: TrainerHelper, proc: train_pb2.HanTrainStatus,
                 option: train_pb2.Trainer, state_dict=None):
        self.helper = helper
        self.proc = proc
        self.option = option

        self.han_trained_home = os.path.join(
            self.conf.get('brain-han.trained.root'),
            self.proc.key
        )
        self.result_file = os.path.join(self.han_trained_home, 'result.txt')

        han_test_home = os.path.join(self.han_trained_home, 'test')
        self.test_header_file = os.path.join(han_test_home, 'header.txt')
        self.test_result_home = os.path.join(han_test_home, 'result')

        if not os.path.exists(self.test_result_home):
            os.makedirs(self.test_result_home)

        resource_home = os.path.join(self.conf.get('brain-han.resource.root'),
                                     lang_pb2.LangCode.Name(self.proc.lang))
        feature_list = FeatureList(resource_home)

        data_home = os.path.join(resource_home, 'train-data')
        self.raw_data_loader = util.RawDataLoader(data_home,
                                                  feature_list,
                                                  option.raw_data_loader)

        logger.info('[load resources]')
        self.proc.step = train_pb2.HAN_LOAD_RESOURCES
        self.helper.save_proc(self.proc)

        self.nlp = util.NlpFactory.make(feature_list, option.nlp,
                                        self.proc.lang)

        self.proc.value += 1
        logger.info('[load nn model]')
        self.proc.step = train_pb2.HAN_LOAD_NN_MODEL
        self.helper.save_proc(self.proc)

        self.model = ClassifierModel(len(option.raw_data_loader.labels),
                                     feature_list, option.model, state_dict)

        self.proc.value += 1

        self.han_data_loader_factory = util.HanDataLoaderFactory(
            self.model.use_gpu, feature_list)

        self.best_acc = 0

        self.make_test_file = partial(
            util.make_test_file,
            label_list=list(self.raw_data_loader.label_list),
            test_result_home=self.test_result_home
        )

    def run(self, data_opt: train_pb2.TrainData):
        graph_maker = util.GraphMaker(
            '{}/{}-{}-{}'.format(self.han_trained_home,
                                 self.proc.model,
                                 lang_pb2.LangCode.Name(self.proc.lang),
                                 self.proc.key)
        ) if data_opt.common.use_graph else None

        self.best_acc = 0

        logger.info('[load raw data]')
        self.proc.step = train_pb2.HAN_LOAD_RAW_DATA
        self.helper.save_proc(self.proc)

        train_data_list, test_data_list = self.raw_data_loader.load_raw_data(
            data_opt)

        self.proc.value += 1
        logger.info('[nlp train data]')
        self.proc.step = train_pb2.HAN_NLP_TRAIN_DATA
        self.helper.save_proc(self.proc)

        self.nlp.make_feature(train_data_list, False)
        train_dataset = HanDataset(train_data_list)
        train_loader = self.han_data_loader_factory.make(
            train_dataset,
            data_opt.common.max_batch_size,
            data_opt.common.train_max_size_prod,
            True
        )

        self.proc.value += 1
        logger.info('[nlp test data]')
        self.proc.step = train_pb2.HAN_NLP_TEST_DATA
        self.helper.save_proc(self.proc)

        self.nlp.make_feature(test_data_list, True)
        test_dataset = HanDataset(test_data_list)
        test_loader = self.han_data_loader_factory.make(
            test_dataset,
            data_opt.common.max_batch_size,
            data_opt.common.test_max_size_prod,
            False
        )

        self.proc.value += 1
        start = datetime.now()
        logger.info('[train run]')
        self.proc.step = train_pb2.HAN_TRAIN_RUN
        self.helper.save_proc(self.proc)

        with open(self.result_file, 'w', encoding='utf-8') as f:
            f.write('train_loss\ttrain_acc\ttest_loss\ttest_acc\n')
        for epoch in range(1, data_opt.common.epoch_size + 1):
            logger.info('Epoch {}'.format(epoch))
            start_time = time.time()

            train_loss, train_acc = self.train(train_loader,
                                               data_opt.common.log_per_update)

            total_time = time.time() - start_time
            doc_per_sec = len(train_loader.dataset) / total_time
            train_time = datetime.now() - start
            logger.info("#doc/sec: {0:.1f} train time: {1}".format(
                doc_per_sec, train_time))

            test_loss, test_acc = self.test(test_loader, epoch)
            with open(self.result_file, 'a', encoding='utf-8') as f:
                f.write('{}\t{}\t{}\t{}\n'.format(
                    train_loss, train_acc, test_loss, test_acc))

            if graph_maker:
                graph_maker.make(epoch, train_loss, train_acc,
                                 test_loss, test_acc)

            self.proc.epoch = epoch
            self.proc.value += 1
            self.helper.save_proc(self.proc)

    def train(self, train_loader, log_per_update):
        while True:
            correct = 0
            start = datetime.now()
            try:
                for i_batch, sample_batched in enumerate(train_loader):
                    correct += self.model.update(sample_batched)
                    del sample_batched
                    if self.model.updates % log_per_update == 0:
                        run_batch = i_batch + 1
                        remain_batch = len(train_loader) - run_batch
                        run_time = datetime.now() - start
                        remaining = run_time * remain_batch / run_batch
                        logger.info(
                            'updates[{0:6}] train loss[{1:.5f}] remaining[{2}]'
                            .format(self.model.updates,
                                    self.model.train_loss.avg,
                                    remaining))
                break
            except RuntimeError as re:
                CudaErrorHandler.handle_error(re, sample_batched[-1],
                                              train_loader)
                del sample_batched
                gc.collect()
                torch.cuda.synchronize()
                continue
        train_loss = self.model.train_loss.avg
        train_acc = correct / len(train_loader.dataset) * 100

        logger.info('#train loss: {0:.5f} acc: {1:.3f}'.format(
            train_loss, train_acc))

        self.model.train_loss.reset()
        return train_loss, train_acc

    def test(self, test_loader, epoch: int):
        while True:
            correct = 0
            result_list = []
            try:
                for sample_batched in test_loader:
                    correct_i, result = self.model.test(sample_batched)
                    correct += correct_i
                    result_list.append((sample_batched, result))
                    del sample_batched
                break
            except RuntimeError as re:
                CudaErrorHandler.handle_error(re, sample_batched[-1],
                                              test_loader)
                del sample_batched
                gc.collect()
                torch.cuda.synchronize()
                continue
        test_loss = self.model.test_loss.avg
        test_acc = correct / len(test_loader.dataset) * 100

        logger.info('#test loss: {0:.5f} acc: {1:.3f}'.format(
            test_loss, test_acc))

        if self.best_acc < test_acc:
            self.best_acc = test_acc
            logger.info('[new best acc: {}]'.format(test_acc))
            model_file_path = os.path.join(self.han_trained_home,
                                           'check_potint_{}.pt'.format(epoch))
            self.model.save(model_file_path, epoch, self.option)
            copyfile(model_file_path, self.helper.pt_file)

            header_list = []
            result_list = [
                util.convert_test_data(sample_batched, result)
                for (sample_batched, result) in result_list
            ]

            pool = Pool()
            try:
                for out in pool.imap_unordered(self.make_test_file, result_list):
                    header_list.extend(out)
            except Exception as e:
                pool.terminate()
                raise e
            finally:
                pool.close()
                pool.join()

            with open(self.test_header_file, 'w') as hf:
                hf.writelines(header_list)

        self.model.test_loss.reset()
        return test_loss, test_acc
