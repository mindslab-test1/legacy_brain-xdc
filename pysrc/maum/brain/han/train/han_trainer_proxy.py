#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import os
import logging
import requests
import json
import glob
import shutil
import signal
from uuid import uuid4
import subprocess

from google.protobuf.timestamp_pb2 import Timestamp

from common.config import Config
from maum.brain.han.train import han_train_pb2 as train_pb2
from maum.brain.han.train.han_trainer_helper import TrainerHelper

_TUTOR_API_KEY = '7c3a778e29ac522de62c4e22aa90c9'
logger = logging.getLogger('root')


class HanTrainerProxy:
    conf = Config()
    """
    Hierarchical Attention Networks Proxy
    """
    uuid = None
    pid = None
    helper = None
    callback_url = None
    fake_cnt = 0
    han_model = train_pb2.HanModel()
    train_result = train_pb2.training
    started = Timestamp()
    subproc = None

    def __init__(self):
        self.dic = {}
        self.uuid = uuid4()
        self.key = str(self.uuid)
        self.model = None

    def __del__(self):
        if self.key is not None:
            self.finish_hook()
            self.key = None

    def run_trainer(self, han_model):
        self.helper.save_model(han_model)
        cmd, args = self.helper.child_command_line()
        env = os.environ.copy()
        env['PATH'] = env['PATH'] + ':/usr/local/cuda/bin'
        logger.info('\nbefore exec: {} {} {}\n'.format(cmd, args, env))
        try:
            cmd_list = [cmd]
            cmd_list += args
            self.subproc = subprocess.Popen(cmd_list)
            self.pid = self.subproc.pid
        except OSError as e:
            logger.critical('execve failed {} {} {} {} {}'.format(cmd, args, env, e.errno, e.strerror))

    def open(self, han_model):
        self.callback_url = han_model.callback_url
        self.model = han_model.model
        """
        실지로 실행하는 학습 프로세스를 실행한다.
        학습 프로세스에서는 학습 실행 결과를 주기적으로 파일에 기록한다.
        빠를 실행 속도를 보장하기 위해서 위와 같이 처리한다.
        파일에 기록할 때 FILE LOCK을 실행한다.
        :param han_model:
        :return:
        """
        self.helper = TrainerHelper(self.uuid, han_model.lang, han_model.model)
        self.run_trainer(han_model)
        logger.info('subprocess: {}'.format(self.pid))

    def finish_hook(self):
        # 파일을 삭제한다.        self.helper.remove_proc()
        res = train_pb2.HanTrainResult.Name(self.train_result)
        logger.info('finish_hook {} {}'.format(res, self.train_result))
        url = "https://{}{}".format(self.callback_url, _TUTOR_API_KEY)
        logger.info('call back url: {}'.format(url))

        cb_msg = {}
        cb_msg['message'] = 'Classifier train job ' + self.key + \
                            ', result:' + res
        cb_msg['data'] = {
            'key': self.key,
            'name': self.model,
            'result': res,
            'binary': self.helper.get_pt_filename()
        }

        body = json.dumps(cb_msg)
        try:
            req = requests.post(url, data=body,
                                headers={'Content-Type': 'application/json'})

            logger.info('RESULT : {}'.format(req.text))
        except Exception as e:
            logger.exception(e)

    def get_key(self):
        return self.key

    def get_pid(self):
        return self.pid

    def get_progress(self):
        proc = self.helper.load_proc()
        if os.path.exists(proc.logfile_path):
            with open(proc.logfile_path, 'r', encoding='utf-8') as f:
                proc.logs.extend(f.readlines())
        try:
            self.train_result = proc.result
        except AttributeError as e:
            logger.error('can not load proc file: {}'.format(e))
        finally:
            # if self.train_result != train_pb2.training:
            #     self.helper.remove_proc()
            logger.debug(proc)
            return proc

    def get_binary(self):
        pt = self.helper.get_pt_file()
        logger.debug(pt)
        if os.path.exists(pt):
            return self.bytes_from_file(pt)
        else:
            return None

    @staticmethod
    def bytes_from_file(filename, chunksize=1024 * 1024):
        with open(filename, "rb") as f:
            while True:
                chunk = f.read(chunksize)
                if chunk:
                    han_train_binary = train_pb2.HanTrainBinary()
                    han_train_binary.pt = chunk
                    yield han_train_binary
                else:
                    break

    # not be used for now
    @staticmethod
    def get_binary_by_key(uuid, context, grpc):
        conf = Config()
        ws = conf.get('brain-han.trained.root')
        trained_model = os.path.join(ws, str(uuid), '*-{}.pt'.format(str(uuid)))
        files = glob.glob(trained_model)
        if len(files) > 0:
            return HanTrainerProxy.bytes_from_file(filename=files[0])
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details('train id not found')
            raise grpc.RpcError("not found")

    @staticmethod
    def remove_binary_by_key(uuid):
        conf = Config()
        trained_dir = os.path.join(conf.get('brain-han.trained.root'), uuid)
        logger.info('remove trained model by key: {}'.format(trained_dir))
        shutil.rmtree(trained_dir)

        ws_dir = os.path.join(conf.get('brain-han.trainer.workspace.dir'))
        logger.info('remove training intermediate artifacts by key')
        for rm_file in glob.glob(os.path.join(ws_dir, '{}*'.format(uuid))):
            try:
                shutil.rmtree(rm_file)
            except NotADirectoryError:
                os.remove(rm_file)

    def cancel(self):
        proc = self.helper.load_proc()
        if proc.result == train_pb2.training:
            logger.debug("self.pid: {}".format(str(self.pid)))
            os.kill(self.pid, signal.SIGTERM)
            self.train_result = train_pb2.cancelled
            proc.result = train_pb2.cancelled
            self.helper.save_proc(proc)

    def set_result(self, res):
        logger.info('SET RESULT {}'.format(train_pb2.HanTrainResult.Name(res)))
        self.train_result = res
