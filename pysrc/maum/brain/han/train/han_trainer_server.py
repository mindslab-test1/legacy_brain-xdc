#!/usr/bin/env python3.5
# -*- coding:utf-8 -*-

import logging
import grpc
import threading
from google.protobuf.empty_pb2 import Empty

from common.config import Config
import maum.brain.han.train.han_train_pb2 as train_pb2
import maum.brain.han.train.han_train_pb2_grpc as train_pb2_grpc
from maum.brain.han.train.han_trainer_proxy import HanTrainerProxy

logger = logging.getLogger('root')


class HanTrainer(train_pb2_grpc.HanTrainerServicer):
    conf = Config()
    trainers = {}
    childs = {}

    """
    Provide methods that implement HAN trainer
    """

    def __init__(self):
        pass

    def Open(self, han_model, context):
        trainer = HanTrainerProxy()
        trainer.open(han_model)

        ret = train_pb2.HanTrainKey()
        ret.train_id = trainer.get_key()
        logger.info('NEW TRAINER {} {}'.format(trainer.get_key(), ret.train_id))
        self.trainers[trainer.get_key()] = trainer
        self.childs[trainer.get_pid()] = trainer
        logger.info(self.childs)
        logger.info(self.trainers)
        return ret

    def GetProgress(self, key, context):
        if key.train_id in self.trainers:
            trainer = self.trainers[key.train_id]
            return trainer.get_progress()
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details('train id not found')
            raise grpc.RpcError("not found")

    # not be used for now
    def GetBinary(self, key, context):
        if key.train_id in self.trainers:
            trainer = self.trainers[str(key.train_id)]
            return trainer.get_binary()
        else:
            return HanTrainerProxy.get_binary_by_key(str(key.train_id), context, grpc)

    def RemoveBinary(self, key, context):
        HanTrainerProxy.remove_binary_by_key(str(key.train_id))
        return Empty()

    def Close(self, key, context):
        if key.train_id in self.trainers:
            trainer = self.trainers[key.train_id]
            ret = trainer.get_progress()
            del self.trainers[key.train_id]
            pid = list(self.childs.keys())[list(self.childs.values()).index(trainer)]
            del self.childs[pid]
            logger.info('NOW delete proxy trainer {} {} {}'.format(pid, key, trainer))
            del trainer
            return ret
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details('train id not found')
            raise grpc.RpcError("not found")

    def Stop(self, key, context):
        id = str(key.train_id)
        logger.debug(self.trainers)
        trainer = self.trainers[id]
        if trainer is not None:
            ret = trainer.get_progress()
            trainer.cancel()
            return ret
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details('train id not found')
            raise grpc.RpcError("not found")

    def GetAllProgress(self, empty, context):
        ret = train_pb2.HanTrainStatusList()
        res = []
        for tr in self.trainers.values():
            res.append(tr.get_progress())
        ret.han_trains.extend(res)
        return ret

    def update_proc(self, proc_stat):
        """
        child에서 종료할 때, 해당 상태를 정리한다.
        :param proc_stat: proc의 상태 값을 전달한다.
        """
        key = str(proc_stat.key)
        if key in self.trainers:
            trainer = self.trainers[key]
            pid = list(self.childs.keys())[list(self.childs.values()).index(trainer)]
            assert pid == proc_stat.pid
            del self.trainers[key]
            del self.childs[pid]
            # 상태를 기록해준다.
            trainer.set_result(proc_stat.result)
            logger.info('NOW delete proxy trainer {} {} {}'.format(pid, key, trainer))
            thread = threading.Thread(target=lambda subproc: subproc.wait(),
                                      args=(trainer.subproc, ))
            thread.daemon = True
            thread.start()
            del trainer
