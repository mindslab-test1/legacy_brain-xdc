#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import os
import sys
import time
import logging
import fcntl

from google.protobuf import timestamp_pb2

from common.config import Config
from maum.brain.han.train import han_train_pb2 as train_pb2
from maum.common import lang_pb2

logger = logging.getLogger('root')


class TrainerHelper:
    conf = Config()

    def __init__(self, uuid, lang, model):
        logger.info('HELPER {} {} {}'.format(uuid, lang, model))
        ws_dir = self.conf.get('brain-han.trainer.workspace.dir')
        han_trained_home = self.conf.get('brain-han.trained.root')

        self.uuid = uuid
        self.lang = int(lang)
        self.model = model

        self.model_file = os.path.join(ws_dir, str(self.uuid) + '.model')
        self.proc_file = os.path.join(ws_dir, str(self.uuid) + '.proc')
        self.pt_filename = '{}-{}-{}.pt'.format(model, lang_pb2.LangCode.Name(self.lang), str(self.uuid))
        self.pt_file = os.path.join(han_trained_home, str(self.uuid), self.pt_filename)

        logger.info(" ".join([ws_dir, str(self.uuid), str(lang), self.model]))
        if not os.path.exists(ws_dir):
            os.makedirs(ws_dir)

    def get_parent_endpoint(self):
        return '127.0.0.1:' + self.conf.get('brain-han.trainer.front.port')

    def get_model_file(self):
        return self.model_file

    def get_proc_file(self):
        return self.proc_file

    def load_model(self):
        try:
            f = open(self.model_file, 'rb')
            han_model = train_pb2.HanModel()
            han_model.ParseFromString(f.read())
            f.close()
            return han_model
        except IOError as e:
            logger.critical('open failed: {} {} {}'.format(self.model_file, e.errno, e.strerror))
            return None

    def save_model(self, model):
        f = open(self.model_file, 'wb')
        f.write(model.SerializeToString())
        f.close()

    def load_proc(self):
        try:
            logger.debug(self.proc_file)
            f = open(self.proc_file, 'rb')
            fcntl.lockf(f.fileno(), fcntl.LOCK_SH)
            proc = train_pb2.HanTrainStatus()
            proc.ParseFromString(f.read())
            fcntl.lockf(f.fileno(), fcntl.LOCK_UN)
            f.close()
            return proc
        except IOError as e:
            logger.critical('open failed: {} {} {}'.format(self.proc_file, e.errno, e.strerror))
            return None

    def save_proc(self, proc):
        # update elapsed time
        now = time.time()
        seconds = int(now)
        nanos = int((now - seconds) * 10 ** 9)
        end = timestamp_pb2.Timestamp(seconds=seconds, nanos=nanos)
        proc.elapsed.seconds = end.seconds - proc.started.seconds
        proc.elapsed.nanos = end.nanos - proc.started.nanos
        if proc.elapsed.seconds > 0 and proc.elapsed.nanos < 0:
            proc.elapsed.seconds -= 1
            proc.elapsed.nanos += 1000000000

        f = open(self.proc_file, 'wb')
        fcntl.lockf(f.fileno(), fcntl.LOCK_EX)
        f.write(proc.SerializeToString())
        fcntl.lockf(f.fileno(), fcntl.LOCK_UN)
        f.close()

    def remove_proc(self):
        if os.path.exists(self.proc_file):
            f = open(self.proc_file, 'wb')
            fcntl.lockf(f.fileno(), fcntl.LOCK_EX)
            fcntl.lockf(f.fileno(), fcntl.LOCK_UN)
            f.close()
            os.remove(self.proc_file)

    def child_command_line(self):
        self.init_proc()
        exe_path = os.path.realpath(sys.argv[0])
        bin_path = os.path.dirname(exe_path)
        exe = os.path.join(bin_path, 'brain-han-train-proc')
        py = 'python'

        return py, [exe,
                    '-u', str(self.uuid),
                    '-l', str(self.lang),
                    '-m', str(self.model)]

    def get_pt_filename(self):
        return self.pt_filename

    def get_pt_file(self):
        return self.pt_file

    def init_proc(self):
        f = open(self.proc_file, 'wb')
        fcntl.lockf(f.fileno(), fcntl.LOCK_EX)

        proc = train_pb2.HanTrainStatus()
        proc.result = train_pb2.preparing

        f.write(proc.SerializeToString())
        fcntl.lockf(f.fileno(), fcntl.LOCK_UN)
        f.close()
