#!/usr/bin/env python3.5
# -*- coding:utf-8 -*-

import os
import sys
import logging
import time
import argparse
import signal
import grpc
import torch

from uuid import UUID

sys.path.append(os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))
import maum.brain.han.utils.general as util
import maum.brain.han.train.han_train_pb2 as train_pb2
import maum.brain.han.train.han_train_pb2_grpc as train_pb2_grpc

from maum.brain.han.train.trainer import Trainer
from maum.brain.han.train.han_trainer_helper import TrainerHelper
from common.config import Config

__mrc_proc__ = None
logger = None  # re-assign in set logger function


class HanTrainProcess:
    conf = Config()
    conf.init('brain-han-train.conf')

    uuid = None
    proc = None
    model = None

    def report_stat(self, result, sig_no):
        parent_remote = self.helper.get_parent_endpoint()
        logger.info('REPORT STAT {}'.format(parent_remote))
        chan = grpc.insecure_channel(parent_remote)
        stub = train_pb2_grpc.TrainerMonitorStub(chan)

        proc_stat = train_pb2.TrainProcStatus()
        proc_stat.key = str(self.uuid)
        proc_stat.pid = os.getpid()
        proc_stat.result = result
        proc_stat.sig_no = sig_no
        stub.Notify(proc_stat)

    def _handle_signal(self, signal, frame):
        self.report_stat(train_pb2.cancelled, signal)
        sys.exit(0)

    def __init__(self, uuid, lang, model):
        signal.signal(signal.SIGTERM, self._handle_signal)
        signal.signal(signal.SIGSEGV, self._handle_signal)
        signal.signal(signal.SIGINT, self._handle_signal)
        signal.signal(signal.SIGBUS, self._handle_signal)

        self.uuid = UUID(uuid)
        self.helper = TrainerHelper(self.uuid, lang, model)
        self.model = self.helper.load_model()

        self.init_proc()
        self.set_logger()

    def calc_model_maximum(self):
        point = 0
        # HAN_LOAD_RESOURCES
        point += 1
        # HAN_LOAD_NN_MODEL
        point += 1
        # HAN_LOAD_RAW_DATA
        point += 1
        # HAN_NLP_TRAIN_DATA
        point += 1
        # HAN_NLP_TEST_DATA
        point += 1
        # HAN_TRAIN_RUN
        point += self.model.data.common.epoch_size
        # HAN_TRAIN_CLEAN
        point += 1
        return point

    def init_proc(self):
        proc = train_pb2.HanTrainStatus()

        proc.result = train_pb2.training
        proc.key = str(self.uuid)
        proc.step = train_pb2.HAN_TRAIN_START
        proc.model = self.model.model
        proc.lang = self.model.lang

        proc.maximum = self.calc_model_maximum()
        proc.value = 0

        proc.option.CopyFrom(self.model.option)
        proc.data.CopyFrom(self.model.data)
        proc.epoch = 0

        proc.started.GetCurrentTime()
        proc.elapsed.seconds = 0
        proc.elapsed.nanos = 0

        self.proc = proc
        self.helper.save_proc(self.proc)

    def set_logger(self):
        os.environ['TZ'] = 'Asia/Seoul'
        time.tzset()
        log_dir = os.path.join(self.conf.get('logs.dir'), 'han', str(self.uuid))
        logfile_name = os.path.join(log_dir, '{}_train.log'.format(time.strftime("%Y%m%d_%H%M%S")))
        self.proc.logfile_path = logfile_name
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)
        util.custom_logger(name='subproc', type='file', fn=logfile_name)
        global logger
        logger = logging.getLogger('subproc')

    def train(self):
        try:
            if self.model.pretrained_model_path:
                pt_path = os.path.join(self.conf.get('brain-han.trained.root'),
                                       self.model.pretrained_model_path)
                pretrained_model = torch.load(pt_path)
                trainer_option = pretrained_model['option']
                state_dict = pretrained_model['state_dict']
            else:
                trainer_option = self.model.option
                state_dict = None
            trainer = Trainer(self.helper, self.proc, trainer_option, state_dict)

            data = self.model.data
            trainer.run(data)

            self.proc.step = train_pb2.HAN_TRAIN_CLEAN
            self.helper.save_proc(self.proc)
            os.remove(self.helper.get_model_file())

            self.proc.value += 1
            self.proc.step = train_pb2.HAN_TRAIN_DONE
            self.helper.save_proc(self.proc)
            self.report_stat(train_pb2.success, 0)
        except Exception as e:
            logger.exception(e)
            self.report_stat(train_pb2.failed, 0)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='classifier trainer executor')
    parser.add_argument('-u', '--uuid',
                        nargs='?',
                        dest='uuid',
                        required=True,
                        help='Unique trainer id')
    parser.add_argument('-l', '--lang',
                        nargs='?',
                        dest='lang',
                        required=True,
                        help='Language (kor, eng)')
    parser.add_argument('-m', '--model',
                        nargs='?',
                        dest='model',
                        required=True,
                        help='Model Name')

    args = parser.parse_args()

    han_proc = HanTrainProcess(args.uuid, args.lang, args.model)
    logger.info('Starting child with: {}'.format(sys.argv))
    __han_proc__ = han_proc
    han_proc.train()
