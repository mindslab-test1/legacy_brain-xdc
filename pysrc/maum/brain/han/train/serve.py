#!/usr/bin/env python3.5
# -*- coding:utf-8 -*-

import logging
import time
from concurrent import futures

import grpc
from maum.brain.han.train.han_train_monitor import HanTrainerMonitor

import maum.brain.han.train.han_train_pb2_grpc as train_pb2_grpc
from maum.brain.han.train.han_trainer_server import HanTrainer
from common.config import Config

_ONE_DAY_IN_SECONDS = 60 * 60 * 24
logger = logging.getLogger('root')


def serve():
    logger.info('han-traind initializing...')
    conf = Config()
    conf.init('brain-han-train.conf')

    svc = HanTrainer()
    mon = HanTrainerMonitor(svc)
    data = [
        ('grpc.max_connection_idle_ms', int(conf.get("brain-han.trainer.front.timeout"))),
        ('grpc.max_connection_age_ms', int(conf.get("brain-han.trainer.front.timeout")))
    ]
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10), None, None, data, None)
    train_pb2_grpc.add_HanTrainerServicer_to_server(svc, server)
    train_pb2_grpc.add_TrainerMonitorServicer_to_server(mon, server)

    port = conf.get('brain-han.trainer.front.port')
    server.add_insecure_port('[::]:' + port)

    logger.info('han-traind starting at 0.0.0.0:' + port)
    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
