#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import logging
import os
import sys

activator = os.path.join(os.environ['HOME'], ".virtualenvs/venv_han/bin/activate_this.py")
with open(activator) as f:
    exec(f.read(), {'__file__': activator})

sys.path.append(os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))
from maum.brain.han.train.serve import serve
import maum.brain.han.utils.general as util

if __name__ == '__main__':
    util.custom_logger('root', 'stream')
    logger = logging.getLogger('root')
    logger.debug('Custom logger is set up.')
    serve()
